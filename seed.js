var env = require('./env'),
    fs = require('fs'),
    dbConfig = require('./config/' + env + '/db'),
    mongoose = require('mongoose-q')();

mongoose.connect(dbConfig.url);
//Все хранится и обрабатывается в UTC
process.env.TZ = 'Europe/Greenwich';

var seedDir = __dirname + '/seeds/';

// Получаем массив сидов, которые надо выполнить
var seedNames = [];
var append = false;
var type = '';
for (var k in process.argv) {
    var arg = process.argv[k];
    if (arg.search('names=') === 0) {
        seedNames.push.apply(seedNames, arg.slice(6).split(','));
    }
    if (arg.search('type=') === 0) {
        type = arg.slice(5).split(',');
        type = type[0] === 'json' ? 'json' : 'js';
    }
    if (arg.search('append=') === 0) {
        append = arg.search('true') > 0;
    }
}

if (!type) {
    console.log('Specify type=[json,js]');
    return;
}

// Функкция применения сида
var seedExecFunction = function (err, data) {
    if (err) {
        console.log('Error: ' + err);
        return;
    }

    var seed = JSON.parse(data);
    //console.log(seed);

    var Schema = require('./app/models/' + seed.model.toLowerCase());

    var iter = 1;
    for (var k in seed.data) {
        var model = new Schema(seed.data[k]);
        if (!append) {
            Schema.removeQ();
        }
        model
            .saveQ()
            .then(function () {
                console.log(seed.model + ' #' + (iter++) + ' saved successfully');
            })
            .catch(function (err) {
                console.log(err);
            })
            .done();
    }

};

// Выполняем все сиды
for (var key in seedNames) {
    if (type === 'json') {
        var file = seedDir + 'json/' + seedNames[key] + '.json';
        fs.readFile(file, 'utf8', seedExecFunction);
    }
    if (type === 'js') {
        var file = seedDir + 'js/' + seedNames[key] + '.js';
        require(file).init(append, env);
    }
}
