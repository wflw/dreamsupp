var env = require('./env'),
    fs = require('fs'),
    dbConfig = require('./config/' + env + '/db');

var sh = require('shelljs');

process.env.TZ = 'Europe/Greenwich';

var migrationDir = __dirname + '/migrations/';

// TODO add concrete migration execute, sort by name
var migrations = fs.readdirSync(migrationDir);

for (var i = 0; i < migrations.length; i++) {
    var execResult = sh.exec(' mongo '+ dbConfig.urlForShellExecute+' ' + migrationDir + migrations[i]);
    console.log(execResult.output);
}
