var jwt = require('jsonwebtoken'),
    events = new (require('events')).EventEmitter(),
    ObjectId = require('mongoose/lib/').Types.ObjectId,
    User = require('./models/user'),

    vObjectId = /^[0-9a-fA-F]{24}$/,
    vInt = /^\d+$/,

    routes = {};
//TODO::Тут сделать цикл по контроллерам, чтобы каждый не прописывать
require('./controllers/test')(routes, events);
require('./controllers/profile')(routes, events);
require('./controllers/auth/vk')(routes, events);
require('./controllers/user')(routes, events);
require('./controllers/report')(routes, events);
require('./controllers/featured')(routes, events);
require('./controllers/courses')(routes, events);
require('./controllers/marathon')(routes, events);
require('./controllers/bills')(routes, events);
require('./controllers/current-group')(routes, events);
require('./controllers/checklist')(routes, events);
require('./controllers/calc')(routes, events);
require('./controllers/feedback')(routes, events);

//TODO::Тут сделать цикл по листнерам, чтобы каждый не прописывать
require('./event-listeners/user')(events);
require('./event-listeners/report')(events);


function getUserRole(userId) {
    return User.findOneQ({
        '_id': ObjectId(userId)
    }, {
        'role': 1
    });
}

function auth() {
    var acceptedRoles = Array.prototype.slice.call(arguments);
    return function (req, res, next) {
        var token = req.headers['x-access-token'],
            decodedToken;
        if (token) {
            decodedToken = jwt.decode(token);

            if (!decodedToken) {
                return res.status(401).json({
                    success: false,
                    errorMessages: [
                        'Некорректный токен'
                    ]
                });
            }
            //exp is in seconds, but Date.now in milliseconds
            if (decodedToken.exp <= (Date.now() / 1000)) {
                return res.status(401).json({
                    success: false,
                    errorMessages: [
                        'Сессия пользователя истекла'
                    ]
                });
            }

            //Идентифактор текущего пользователя для использования в конкретных роутах
            req.userId = decodedToken._id;
            getUserRole(req.userId)
                .then(function (user) {
                    if (user) {
                        req.userRole = user.role;
                        if (acceptedRoles.length > 0) {
                            if (acceptedRoles.indexOf(req.userRole) === -1) {
                                res.status(403).json({
                                    success: false,
                                    errorMessages: [
                                        'У пользователя недостаточно привелегий'
                                    ]
                                });
                            }
                        }
                    } else {
                        res.status(401).json({
                            success: false,
                            errorMessages: [
                                'Пользователя не существует'
                            ]
                        });
                    }
                }).then(next);
        } else {
            res.status(401).json({
                success: false,
                errorMessages: [
                    'Для работы с сервисом необходим токен'
                ]
            });
        }
    };
}

var softAuth = function (req, res, next) {
    var userSetMetaQ,
        token = req.headers['x-access-token'],
        decodedToken = {};
    if (token) {
        decodedToken = jwt.decode(token);
        if (!decodedToken) {
            return res.status(401).json({
                success: false,
                errorMessages: [
                    'Некорректный токен'
                ]
            });
        }
        //exp is in seconds, but Date.now in milliseconds
        if (decodedToken.exp <= (Date.now() / 1000)) {
            return res.status(401).json({
                success: false,
                errorMessages: [
                    'Сессия пользователя истекла'
                ]
            });
        }

        req.userId = decodedToken._id;
        userSetMetaQ = getUserRole(req.userId)
            .then(function (user) {
                if (user) {
                    req.userRole = user.role;
                }
            });
    }
    if (userSetMetaQ) {
        userSetMetaQ
            .then(next);
    } else {
        next();
    }
};

// attach routes
module.exports = function (app) {
    app.get('/api/test', auth(), routes.test.get);

    app.param('courseId', vObjectId);
    app.get('/api/auth/vk/:courseId?', routes.auth.vk.start);

    app.get('/api/auth/vk/callback', routes.auth.vk.callback);

    app.get('/api/profile', auth(), routes.profile.get);

    app.get('/api/user/list', auth('coach'), routes.user.getAll);

    app.param('perPage', vInt);
    app.param('page', vInt);
    app.get('/api/featured/:perPage', routes.featured.get);

    app.param('userId', vObjectId);
    app.get('/api/userProgress/:userId/:courseId/days/:perPage/:page', softAuth, routes.courses.getDays);

    app.param('programId', vObjectId);
    app.get('/api/course/nearest/:programId', routes.courses.getNearest);

    //Получение списка курсов, которые еще не стартанули и ждут даты начала или наполнения группы
    app.get('/api/course/waiting', auth('coach'), routes.courses.getNotStarted);
    // Получение списка всех курсов
    app.get('/api/course/all', auth('coach'), routes.courses.getAll);

    //Ручной запуск курса
    app.post('/api/course/start/:courseId',auth('coach'), routes.courses.startCourse);

    app.get('/api/marathon/:courseId', auth(), routes.marathon.getLastStarted);
    app.put('/api/marathon/addUserDay', auth('coach'), routes.marathon.addUserDay);
    app.put('/api/marathon/removeUserDay', auth('coach'), routes.marathon.removeUserDay);

    // Работа с чеклистами
    app.put('/api/checklist/:checklistId', auth(), routes.checklist.update);

    //Запрос на создание счета
    app.get('/api/bill/create/:courseId', auth(), routes.bills.create);

    //API возврата пользователя с платежного шлюза
    app.param('transactionId', vObjectId);
    app.get('/api/bill/success', routes.bills.success);
    app.get('/api/bill/failure', routes.bills.failure);
    app.get('/api/bill/reject', routes.bills.reject);
    app.get('/api/bill/inProgress', routes.bills.inProgress);

    //API взаимодействия с платежным шлюзом за сценой TODO:(!!!Нужно защитить ограничением по IP)
    app.post('/api/bill/check', routes.bills.check);
    app.post('/api/bill/confirm', routes.bills.confirm);

    //API вывода группы (срочная версия)
    // TODO разделить для вошедших пользователей и нет
    app.get('/api/group/active', softAuth, routes.groups.active);

    //API регистрации в группу (срочная версия)
    app.get('/api/group/join/:courseId', auth(), routes.groups.join);

    //API выдачи списка групп в которые входит или входил пользователь и к истории и прогрессу которых он имеет доступ
    app.get('/api/groups/my', auth(), routes.groups.my);

    //API выдачи списка участников и их успехов по марафону
    app.get('/api/group/my/:courseId/:perPage', auth(), routes.groups.membersProgress);

    //API выдачи списка текущих отчетов на проверку
    app.get('/api/report', auth('coach'), routes.report.notVerified);
    app.post('/api/report/respond', auth(), routes.report.respond);
    app.put('/api/report/save', auth(), routes.report.save);
    app.put('/api/report/markCheckNeeded', auth(), routes.report.markCheckNeeded);
    app.put('/api/report/unCheckNeeded', auth(), routes.report.markCheckNotNeeded);

    // feedback
    app.post('/api/feedback', auth(), routes.feedback.create);

    //Насильная оплата пользователем всего курса
    app.get('/api/user/:userId/acceptToCourse/:courseId', auth('coach'), routes.user.acceptToCourse);
    app.get('/api/user/:userId/rejectFromCourse/:courseId', auth('coach'), routes.user.rejectFromCourse);

    // Слияение профилей пользователей с сохранением курсов и истории оплаты
    app.post('/api/user/merge',auth('coach'), routes.user.mergeProfiles);


    //API калькулятора достижения цели
    app.post('/api/calc', auth(), routes.calc.process);

    //API сохранения целей марафона
    app.post('/api/targets/:courseId', auth(), routes.marathon.saveTargets);
};
