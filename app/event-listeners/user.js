var moment = require('moment'),
    util   = require('util'),
    appConfig = require('../../config/' + app.get('env') + '/app'),

    User = require('../models/user'),
    Course = require('../models/course'),
    Program = require('../models/program'),

    vkPh = require('../components/vkphantom');

function getUserSocialProfile(userId, networkName) {
    return User.findOneQ({
        '_id': userId,
        'socialProfiles.networkName': networkName
    }, {
        'socialProfiles.$': 1
    });
}

var addToVKFriend = function (userId) {
    // TODO do it only if user is not in friends
    getUserSocialProfile(userId, 'vk')
        .then(function (user) {
            if (user) {
                vkPh.execute(vkPh.addFriendAction, {
                    vkUserId: user.socialProfiles[0].profileId
                })
                    .then(function () {
                        console.log('VK friend request posted to ' + user.socialProfiles[0].profileUrl + ' ' + user.socialProfiles[0].profileId);
                    })
                    .catch(function (err) {
                        console.log('Vk friend request processed with error ' + err);
                    });
            }
        });
};

function sendVKMessage(userId, message) {
    getUserSocialProfile(userId, 'vk')
        .then(function (user) {
            if (user) {
                vkPh.execute(vkPh.messageAction, {
                    messageText: message,
                    vkUserId: user.socialProfiles[0].profileId
                })
                    .then(function () {
                        console.log('VK message "' + message + '" posted to ' + user.socialProfiles[0].profileUrl + ' ' + user.socialProfiles[0].profileId);
                    })
                    .catch(function (err) {
                        console.log('Vk message processed with error ' + err);
                    });
            }
        });
}

function sendCourseMessage(userId, courseId, action, message, type) {
    return Course.find({
        '_id': courseId
    }, {
        'dateStart': 1,
        'dateEnd': 1,
        'program': 1
    }).populate({
        path: 'program',
        select: {
            title: 1,
            imageUrl: 1
        }
    }).execQ()
        .spread(function (course) {
            User.findQ({
                '_id': userId
            },{
                'profile.firstName': 1
            }).spread(function(user){
                if (user){
                    var nMessage = message;
                    if (action === 'register'){
                        nMessage = util.format(message,
                            user.profile.firstName,
                            course.program.title,
                            moment(course.dateStart).format('DD.MM.YYYY'),
                            moment(course.dateEnd).diff(moment(course.dateStart), 'days'),
                            appConfig.urls.conditions,
                            appConfig.urls.cost
                        );
                    } else if (action === 'payment'){
                        nMessage = util.format(message,
                        course.program.title,
                        moment(course.dateStart).locale('ru').format('Do MMM'),
                        moment(course.dateStart).diff(moment(), 'days'),
                        moment(course.dateStart).add(-1, 'days').locale('ru').format('Do MMM'),
                        appConfig.urls.maraphon
                      );
                    }
                    sendVKMessage(userId, nMessage);
                    return User.updateQ({
                        '_id': userId
                    }, {
                        '$addToSet': {
                            'notifications': {
                                type: type,
                                message: nMessage
                            }
                        }
                    });
                } else {
                    return user;
                }
            });
        });
}

function addAcceptNotification(userId, courseId) {
    sendCourseMessage(userId, courseId, 'payment', 'Спасибо за оплату участия марафоне достижения целей «%s». Мы стартуем %s, всего через %s дней. За день до старта, %s, ждем Вас тут %s Первое задание появится в 19.00. До скорой встречи :)  Если у Вас возникнут вопросы - задавайте. С радостью отвечу.', 'success');
}

function addRejectNotification(userId, courseId) {
    //sendCourseMessage(userId, courseId, 'Вам отказано в участии в марафоне :', 'info'); //В случае отказа ничего писать не будем, а то забанят еще :)
}

function addMemberNotification(userId, courseId) {
    addToVKFriend(userId);
    sendCourseMessage(userId, courseId, 'register', 'Дорогая %s, Вы зарегистированы в марафоне достижения целей «%s», который стартует %s и продлится %s дней.\r\n Прочитать про условия участия можно тут: %s\r\n Про стоимость и порядок оплаты тут: %s\r\n Способ оплаты можно уточнить у меня. \r\n Если у Вас еще есть вопросы, задавайте, я с радостью на них отвечу!', 'info');
}

module.exports = function (events) {
    events.on('userAcceptedToCourse', addAcceptNotification);
    events.on('userRejectedFromCourse', addRejectNotification);
    events.on('userAddedToCourse', addMemberNotification);
};
