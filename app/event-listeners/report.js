var util = require('util'),

    User = require('../models/user'),
    Course = require('../models/course'),
    Program = require('../models/program'),

    vkPh = require('../components/vkphantom');

function getUserSocialProfile(userId, networkName) {
    return User.findOneQ({
        '_id': userId,
        'socialProfiles.networkName': networkName
    }, {
        'socialProfiles.$': 1
    });
}

function sendVKMessage(userId, message) {
    getUserSocialProfile(userId, 'vk')
        .then(function (user) {
            if (user) {
                vkPh.execute(vkPh.messageAction, {
                    messageText: message,
                    vkUserId: user.socialProfiles[0].profileId
                })
                    .then(function () {
                        console.log('VK message "' + message + '" posted to ' + user.socialProfiles[0].profileUrl + ' ' + user.socialProfiles[0].profileId);
                    })
                    .catch(function (err) {
                        console.log('Vk message processed with error ' + err);
                    });
            }
        });
}

function sendReportMessage(userId, courseId, dayNumber, message, template, type) {
    return Course.find({
        '_id': courseId
    }, {
        'program': 1
    }).populate({
        path: 'program',
        select: {
            title: 1
        }
    }).execQ()
        .spread(function (course) {
            User.findQ({
                '_id': userId
            }, {
                'profile.firstName': 1
            }).spread(function (user) {
                if (user) {
                    var nMessage = util.format(template,
                        user.profile.firstName,
                        dayNumber,
                        course.program.title,
                        message);

                    sendVKMessage(userId, nMessage);
                    return User.updateQ({
                        '_id': userId
                    }, {
                        '$addToSet': {
                            'notifications': {
                                type: type,
                                message: nMessage
                            }
                        }
                    });
                } else {
                    return user;
                }
            });
        });
}

function addReportAcceptedNotification(userId, courseId, dayNumber, message) {
    User.findOne({_id: userId})
        .populate('courses.course')
        .execQ()
        .then(function (user) {
            // TODO user can have many groups
            for (var i = 0; i < user.courses.length; i++) {
                var course = user.courses[i].course;
                if (String(course._id) === String(courseId)) {
                    for (var j = 0; j < user.courses[i].days.length; j++) {
                        var day = user.courses[i].days[j];
                        if (+day.dayNumber === +dayNumber) {
                            vkPh.execute(vkPh.postGroupAction, {
                                messageText: '@id' + user.socialProfiles[0].profileId + "\r\nДень " + dayNumber + "\r\n" + day.report,
                                groupId: course.socialGroups[0].groupId
                            })
                                .then(function () {
                                    console.log('event listener report accepted, report posted in group');
                                    sendReportMessage(userId, courseId, dayNumber, message, '%s, Ваш отчет по %d дню марафона «%s» опубликован с комментарием\r\n%s', 'success');
                                })
                                .catch(function (err) {
                                    console.log('Vk group post processed with error ' + err);
                                });
                            break;
                        }

                    }
                    break;
                }

            }
        })
        .catch(function (err) {
            console.log(err);
        });
}

function addReportRejectedNotification(userId, courseId, dayNumber, message) {
    sendReportMessage(userId, courseId, dayNumber, message, '%s, Ваш отчет по %d дню марафона «%s» был отклонен\r\n%s', 'error');
}

function addReportMessageNotification(userId, courseId, dayNumber, message) {
    sendReportMessage(userId, courseId, dayNumber, message, '%s, у меня вопрос по отчету за %d день марафона «%s»\r\n%s', 'info');
}

module.exports = function (events) {
    events.on('reportMessage', addReportMessageNotification);
    events.on('reportRejected', addReportRejectedNotification);
    events.on('reportAccepted', addReportAcceptedNotification);
};
