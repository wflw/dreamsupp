var mongoose = require('mongoose/lib/');
var Schema = mongoose.Schema;

var ChecklistSchema = new Schema({ //Cписок детальных действий в день марафона
    user: {type: Schema.Types.ObjectId, ref: 'User'}, //Пользователь, чей лист
    course: {type: Schema.Types.ObjectId, ref: 'Course', required: true}, //Курс, чей лист
    dayNumber: {type: Number, required: true}, //К какому дню относится
    list: {
        pinned: [{
            text: String, //Сожержимое единицы списка
            isDone: Boolean //Явялется ли сделаным
        }],
        mandatory: [{
            text: String,
            isDone: Boolean
        }],
        simple: [{
            text: String,
            isDone: Boolean
        }]
    }
});

module.exports = mongoose.model('Checklist', ChecklistSchema);