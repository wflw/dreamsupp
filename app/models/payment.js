var mongoose = require('mongoose/lib/');
var Schema = mongoose.Schema;

var PaymentSchema = new Schema({
    providerName: String, //Имя аггрегатора, который использовался для платежа
    user: {type: Schema.Types.ObjectId, ref: 'User'},//Пользователь, чей платеж
    course: {type: Schema.Types.ObjectId, ref: 'Course'}, //Курс за который оплачивается
    days: [Number],// Дни, за которые осуществляется или осуществлена оплата.
    created: {type: Date, default: Date.now, required: true}, //Время инициации платежа
    closed: Date,  //Время и признак закрытия платежа (совершен, отвергнут)
    retries:[{ //Повторные отправки платежа на шлюз
        id: {type: Schema.Types.ObjectId, required: true},
        sendDate: {type: Date, default: Date.now, required: true}, //Дата отправки на шлюз
        receiveDate: Date //Дата получения ответа со шлюза
    }],
    transactions: [{ //Реальные транзакции на платежных шлюзах по платежу
        id: String,//Идентификатор транзакции аггрегатора
        innerProvider: String, //Идентификатор конкретного платженого шлюза (если его отдает аггрегатор)
        amount: Number, //Количество денег
        currency: String, //Валюта платежа
        sent: Date, //Дата отправки платежа на шлюз
        accepted: Date, //Дата успешного приема платежа (может не быть, если был отклонен)
        rejected: Date, //Дата отклонения платежа (может и не быть, если платеж был успешно принят)
        failed: Date //Дата неуспешной попытки оплаты
    }]
});

module.exports = mongoose.model('Payment', PaymentSchema);