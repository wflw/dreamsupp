var mongoose = require('mongoose/lib/'),
    Schema = mongoose.Schema;

var FeedbackSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    course: {type: Schema.Types.ObjectId, ref: 'Course'},
    items: [{
        question: {type: String},
        answer: {type: String}
    }]
});

module.exports = mongoose.model('Feedback', FeedbackSchema);
