var mongoose = require('mongoose/lib/');
var Schema = mongoose.Schema;

var CourseSchema;
CourseSchema = new Schema({
    program: {type: Schema.Types.ObjectId, ref: 'Program', required: true}, //Программа по которой идет курс
    created: {type: Date, default: Date.now, required: true},   //Когда был создан
    dateStart: {type: Date, required: true}, //Когда планирует быть начат
    dateEnd: {type: Date, required: true},   //Когда планирует завершиться
    socialGroups: //Соотвтетвующие курсу "группы" в соцсетях
    [{
        networkName: String, //Имя соцсети
        groupId: String,     // Id группы понятное соцести (для API)
        groupUrl: String     // Понятный браузеру URL группы
    }],
    members: [//Участники курса
        {
            user: {type: Schema.Types.ObjectId, ref: 'User'}, //Пользователь
            dateJoined: Date //Дата присоедниения пользователя к курсу, если пустое, значит еще не присоединен/не оплатил курс
        }
    ],
    isStarted: Boolean, //Признак того, что группа была собрана (по наполнению или по времени) и по-сути запущена
    memberCount: {type: Number, default: 0} //Текущее количество участников в группе
});

module.exports = mongoose.model('Course', CourseSchema);