var mongoose = require('mongoose/lib/');
var Schema = mongoose.Schema;

var UserSchema;
var CommentSchema = new Schema();

CommentSchema.add({ //Комментарий
    moderator: {type: Schema.Types.ObjectId, ref: 'User'}, /* Пользователь-модератор, который оставил сообщение.
     Если не выставлено, значит сам пользователь оставил комментарий по этому полю определяется смещение комментария*/
    text: String,                          //Текст комметария
    date: Date, //Время появления комментария, по ней определяется порядок коментария
    verb: {type: String, enum: ['m','r','a']} //Системный смысл сообщения (m = просто сообщение,r = отказ публикации отчета, a = подтверждения сдачи отчета)
});

UserSchema = new Schema({
    pos: Number,//На случай ручной сортировки
    role: {type: String, enum: ['coach', 'student']},
    profile: { //Профиль пользователя
        firstName: String, //Имя
        lastName: String,  //Фамилия
        avatarUrl: String, //Аватар
        email: String,     //Email
        timeZone: String,  //Временная зона. Нужна для вычисления когда выдавать следующее задание
        country: String,   //Страна проживания
        city: String,      //Город. По нему можно вычислить временную зону. Его и будем спрашивать или угадывать
        age: Number,       //Возраст пользователя
        birthday: String       //Дата рождения
    },
    socialProfiles: [
        { //Профили данного пользователя в социальных сетях
            networkName: {type: String, enum: ['vk', 'fb']}, //Наименование социальной сети
            profileId: String,   //Уникальный идентификатор профиля в социальной сети, понимаемая драйвером этой сети
            profileUrl: String   //Полная ссылка на профиль в социальной сети
        }
    ],
    courses: [
        {
            //Марафоны на которые был или еще зачислен пользователь
            course: {
                type: Schema.Types.ObjectId,
                ref: 'Course'
            }, /*Идентификатор конкретного марафона, в котором учавствует или
         учавствовал пользователь.*/
            isFeatured: Boolean, //Признак разрешения пользвователем размещать его прохождение курса на "глагне" :)
            dateJoined: Date, //Дата присоединения к марафону (он же признак участия/оплаты)
            dateExited: Date, /*Дата выхода из марафона (может быть и насильный выход, не по окончанию всего марафона для всех) */
            target: String, /*Намеченная участником цель на марафон*/
            targets: [{
                name: {type:String, required: true},
                daysToReach: {type:Number, required: true},
                checked: {type:Boolean, required: true}
            }],
            days: [
                {
                    /*Дни, которые предполагаются у пользователя при прохождении марафона.
                     Создаются сразу при входе на курс */
                    dayNumber: {type: Number, required: true},
                    title: String,
                    description: String,
                    quest: String,
                    questForMyself: String,
                    successDiary: String,
                    checklist: {type: Schema.Types.ObjectId, ref: 'Checklist'}, /*Идентификатор чеклиста,
                 с которым работал пользователь в этот конкретный день*/
                    reportLink: String, //Ссылка на загруженный отчет о проделаноой работе
                    report: String, //Отчет текстом в нашей системе
                    isPurchased: Boolean, /*Признак того, что пользователь оплатил день,
                 в случае с покупками в мобилках может быть подневная покупка (InApp) */
                    isAccepted: Boolean, //Был ли успешно проверен и засчитан отчет и закончен день
                    needToBeChecked: Boolean,//Признак отправки на проверку модератору
                    comments: [ //Переписка модератора и пользователя
                        CommentSchema
                    ],
                    percentDone: {type: Number, min: 0, max:100} //Процент отмеченых галок в чеклисте
                }
            ],
            purchaseHistory: [{ //История покупок
                dayNumber: Number, //День за который была произведена оплата, если не выставлено, значит за весь курс
                payment: {type: Schema.Types.ObjectId, ref: 'Payment'},//Конкретный платеж
                closed: Date //Дата закрытия платежа (когда был отклонен или реально оплачен)
            }],
            finalReportLink: String, //Ссылка на финальный отчет на внешнем ресурсе
            finalReport: String //Финальный отчет в тестовом виде
        }
    ],
    notifications : [
        {
            type: {type: String, required: true, enum: ['error', 'info', 'success']},
            message:String
        }
    ]
});

module.exports = mongoose.model('User', UserSchema);
