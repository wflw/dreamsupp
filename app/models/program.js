var mongoose = require('mongoose/lib/');
var Schema = mongoose.Schema;

var MoneySchema = new Schema({ //Стоимость
    currency: String, //Валюта
    amount: Number    //Цена
});

var ProgramSchema = new Schema(
    {//Программы обучения
        title: String, //Название программы
        imageUrl: String,//Картинка программы
        fullCost: [MoneySchema], //Полная базовая стоимость программы по валютам
        description: String, //Распространненое описание курса
        days: [
            {
                dayNumber: Number,               //Номер дня программы
                title: String,
                description: String,
                quest: Schema.Types.Mixed, //Задание в данный день
                questForMyself: String,
                successDiary: String,
                dayCost: [MoneySchema],     //Стоимость одного дня
                checklist: [String]
            }
        ]
    }
);

module.exports = mongoose.model('Program', ProgramSchema);
