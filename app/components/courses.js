var q = require('q'),
    ObjectId = require('mongoose/lib/').Types.ObjectId,

    appConfig = require('../../config/' + app.get('env') + '/app'),

    User = require('../models/user'),
    Course = require('../models/course');


module.exports = {
    addUserToCourseQ: function (userId, courseId) {
        if (!courseId.isValid) {
            courseId = ObjectId(courseId);
        }
        if (!userId.isValid) {
            userId = ObjectId(userId);
        }
        var courseUpd = Course.updateQ(
            {
                '_id': courseId, //Ищем курс
                'members.user': {$nin: [userId]}/*, //с учетом того, что участник в нем уже не находится
             'memberCount': {$lt: appConfig.courseMembersMax} //Наполнение сейчас нас не волнует // и что в нем нет наполнения */
            }, {
                '$inc': {'memberCount': 1}, //Сразу увеличиваем счетчик участиков
                '$addToSet': {
                    'members': {
                        'user': userId //И самого участника добавим
                    }
                }
            });
        var userUpd = User.updateQ(
            {
                '_id': userId, //Ищем курс
                'courses.course': {$nin: [courseId]} //с учетом того, что участник в нем уже не находится
            }, {
                '$addToSet': {
                    'courses': {
                        'course': courseId //И самого участника добавим
                    }
                }
            });
        return q.all([courseUpd, userUpd]);
    },
    getUserActiveCoursesQ: function (userId) {
        return User.find({
            '_id': ObjectId(userId),
            'courses.dateJoined': {'$exists': true}
        }).select({
            'courses.dateJoined': 1,
            'courses.course': 1,
            'courses.dateExited': 1,
            'courses': {
                '$elemMatch': {
                    'dateJoined': {'$exists': true}
                }
            }
        }).sort({
            'dateExited': 1,
            'dateJoined': -1
        }).execQ()
            .spread(function (user) {
                return user;
            });
    }
};
