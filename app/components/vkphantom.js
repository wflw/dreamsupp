var phantom = require('phantom'),
    q = require('q'),

    config = require('../../config/' + app.get('env') + '/app'),
    userAgent = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)';

// TODO move click somewhere too much copypaste
//function click(el) {var ev = document.createEvent('MouseEvent'); ev.initMouseEvent('click', true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null); el.dispatchEvent(ev);}

/* How to use this shit
 var vkPh = require('./app/controllers/vkphantom');
 vkPh.execute(vkPh.postGroupAction, {messageText: 'Просто много текста, чтобы проверить', groupId: '79528002'})
 .then(function() {
 console.log('Posted ');
 });

 var vkPh = require('./app/controllers/vkphantom');
 vkPh.execute(vkPh.addFriendAction, {vkUserId: 125112070})
 .then(function() {
 console.log('Posted ');
 });
 */

var loginMethod = function (page, params, deferred, execResult, ph) {
    console.log('Phantom loginMethod');
    page.evaluate(function (config) {
        var email = document.querySelector('input[name="email"]');
        if (email) {
            email.value = config.vk.user;
        } else {
            return 'No email field found in DOM';
        }

        var pass = document.querySelector('input[name="pass"]');
        if (pass) {
            pass.value = config.vk.pass;
        } else {
            return 'No password field found in DOM';
        }

        var submitButton = document.querySelector('.wide_button');
        if (!submitButton) return 'No submit button found in dom';

        // определяем фцнкцию клика, чтобы тыкать
        function click(el) {
            var ev = document.createEvent("MouseEvent");
            ev.initMouseEvent("click", true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
            el.dispatchEvent(ev);
        }

        click(submitButton);
    }, function (result) {
        if (result) {
            ph.exit();
            return deferred.reject(result);
        }
    }, config);
};

var locationApproveMethod = function (page, params, deferred, execResult, ph) {
    console.log('Phantom locationApproveMethod');
    page.evaluate(function (config) {
        var codeInput = document.querySelector('input[name="code"]');
        if (codeInput) {
            codeInput.value = config.vk.user.substr(1, 8);

            var submitButton = document.querySelector('.button[type="submit"]');
            if (!submitButton) return {error:'No submit button found in dom'};

            // определяем фцнкцию клика, чтобы тыкать
            function click(el) {
                var ev = document.createEvent("MouseEvent");
                ev.initMouseEvent("click", true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                el.dispatchEvent(ev);
            }

            click(submitButton);

            return {success: 'approved successfully'};
        } else {
            return {success: 'No need to approve'};
        }
    }, function (result) {
        if (result.error) {
            ph.exit();
            return deferred.reject(result.error);
        }
        console.log('Phantom locationApproveMethod ' + result.success);
        page.reload();
    }, config);
};


module.exports.execute = function (action, params) {
    var deferred = q.defer(),
        execResultG = {};

    var next = function (page, params, deferred, execResult, ph) {
        var nextMethod = queue.shift();
        if (!nextMethod) {
            return deferred.reject(new Error('Queue is empty, do smth!'));
        }
        (nextMethod)(page, params, deferred, execResult, ph);
    };

    var phantom = require('phantom'),
        queue = action.queue.slice(0),
        nextCallback = action.next ? action.next : next;

    // Here we add final methon, that closes phantom
    queue.push(function (page, params, deferred, execResult, ph) {
        console.log('Phantom exiting with result ', execResult);
        ph.exit();
        deferred.resolve(execResult);
    });

    // HACK because phantom or vk redirects for the first time
    queue.unshift(function () {
    });

    phantom.create('--ssl-protocol=any', '--web-security=no', '--load-images=no', function (ph) {
        ph.createPage(function (page) {
            page.set('settings.userAgent', userAgent);
            page.set('onLoadFinished', function () {
                nextCallback(page, params, deferred, execResultG, ph);
            });
            page.set('onError', function (msg) {
                console.log('ERROR ' + msg);
            });
            //page.set('onUrlChanged', function (newUrl) {
            //    console.log('New url  ' + newUrl);
            //});

            page.open('http://m.vk.com/login');
        });

    });

    return deferred.promise;
};

/*
 {groupTitle: 'Новая группа'}
 */
module.exports.createGroupAction = {
    queue: [
        loginMethod,
        locationApproveMethod,
        // переходим на страницу создания новой группы
        function (page) {
            page.evaluate(function () {
                window.location.href = 'http://m.vk.com/groups?act=new';
            });
        },
        // создаем группу
        function (page, params, deferred, execResult, ph) {
            // HACK группа создается ажаксом, потому нам надо следить за полученными ответами с сервера
            page.set('onResourceReceived', function (resourse) {
                if (resourse.url.indexOf('club') > 0) {
                    page.evaluate(function (newUrl) {
                        window.location.href = newUrl;
                    }, function () {
                    }, resourse.url);
                    page.set('onResourceReceived', undefined);
                }
            });

            page.evaluate(function (groupTitle) {
                    var textfield = document.querySelector('.textfield[name="title"]'),
                        button = document.querySelector('.button[type="submit"]');

                    if (!textfield) return 'No textfield';
                    if (!button) return 'No button';

                    textfield.value = groupTitle;

                    function click(el) {
                        var ev = document.createEvent("MouseEvent");
                        ev.initMouseEvent("click", true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                        el.dispatchEvent(ev);
                    }

                    click(button);
                }, function (result) {
                    if (result) {
                        ph.exit();
                        deferred.reject(result);
                    }
                },
                params.groupTitle);
        },
        // изменяем видимость группы
        function (page, params, deferred, execResult, ph) {
            page.evaluate(function () {
                var radio = document.querySelector('.radio[name="access"]'),
                    button = document.querySelector('.button[type="submit"]');

                if (!radio) return 'No radio';
                if (!button) return 'No button';

                radio.value = 1;

                function click(el) {
                    var ev = document.createEvent("MouseEvent");
                    ev.initMouseEvent("click", true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                    el.dispatchEvent(ev);
                }

                click(button);
                return window.location.href;
            }, function (result) {
                if (result.indexOf('http') === 0) {
                    execResult.groupId = result.split('?')[0];
                    // Is meant to go to final method in query, that returns promise
                    page.reload();
                } else {
                    ph.exit();
                    return deferred.reject(result);
                }
            });
        }

    ]
};

/*
 {groupId: '65413246', vkUserId: '4601557'}
 */
module.exports.inviteFriendAction = {
    queue: [
        loginMethod,
        locationApproveMethod,
        function (page, params) {
            page.evaluate(function (groupId) {
                window.location.href = 'http://vk.com/friends?act=invite&group_id=' + groupId;
            }, undefined, params.groupId);
        },
        function (page, params, deferred, execResult, ph) {
            page.evaluate(function (userId) {
                    var link = document.querySelector('#user_block' + userId).querySelector('.friends_act');
                    if (!link) return 'No link found';

                    function click(el) {
                        var ev = document.createEvent('MouseEvent');
                        ev.initMouseEvent('click', true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                        el.dispatchEvent(ev);
                    }

                    click(link);
                },
                function (result) {
                    if (result) {
                        ph.exit();
                        return deferred.reject(result);
                    }
                    // Is meant to go to final method in query, that returns promise
                    page.reload();
                },
                params.vkUserId
            );
        }
    ]
};

module.exports.postGroupAction = {
    queue: [
        loginMethod,
        locationApproveMethod,
        function (page, params) {
            console.log('phantom go to group page action');
            page.evaluate(function (groupId) {
                window.location.href = 'http://m.vk.com/' + groupId;
            }, undefined, params.groupId);
        },
        function (page, params, deferred, execResult, ph) {
            console.log('phantom posting in group');
            page.evaluate(function (messageText) {
                    var textField = document.querySelector('.textfield[name="message"]');
                    var button = document.querySelector('.button[type="submit"]');

                    if (!textField) return 'No field found';
                    if (!button) return 'No button';

                    textField.value = messageText;

                    function click(el) {
                        var ev = document.createEvent('MouseEvent');
                        ev.initMouseEvent('click', true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                        el.dispatchEvent(ev);
                    }

                    click(button);
                },
                function (result) {
                    if (result) {
                        ph.exit();
                        return deferred.reject(result);
                    }
                    // Is meant to go to final method in query, that returns promise
                    page.reload();
                },
                params.messageText
            );
        }
    ]
};

/*
 {vkUserId: '4601557'}
 */
module.exports.addFriendAction = {
    queue: [
        loginMethod,
        locationApproveMethod,
        // go to user page
        function (page, params) {
            console.log('Phantom addFriendAction go to user page method');
            page.evaluate(function (userId) {
                window.location.href = 'http://m.vk.com/id' + userId;
            }, function () {
            }, params.vkUserId);
        },
        // add user to friends
        function (page, params, deferred, execResult, ph) {
            console.log('Phantom addFriendAction add user as friend action');
            page.evaluate(function () {
                // определяем фцнкцию клика, чтобы тыкать
                function click(el) {
                    var ev = document.createEvent('MouseEvent');
                    ev.initMouseEvent('click', true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                    el.dispatchEvent(ev);
                }

                var buttons = document.querySelectorAll('.wide_button');
                var addButton;
                for (var i = 0; i < buttons.length; i++) {
                    if (buttons[i].innerText === 'Добавить в друзья') {
                        addButton = buttons[i];
                        break;
                    }
                }
                if (!addButton) return 'No submit button found in dom';
                click(addButton);
            }, function (result) {
                if (result) {
                    ph.exit();
                    return deferred.reject(result);
                }
                // Is meant to go to final method in query, that returns promise
                page.reload();
            });
        }
    ]
};

/*
 {messageText: 'Просто много текста, чтобы проверить', vkUserId: '4601557'}
 */
module.exports.messageAction = {
    queue: [
        loginMethod,
        locationApproveMethod,
        // go to writeMessage page
        function (page, params) {
            console.log('Phantom messageAction go to user dialog page method');
            page.evaluate(function (userId) {
                window.location.href = 'http://m.vk.com/write' + userId;
            }, function () {
            }, params.vkUserId);
        },
        // write message
        function (page, params, deferred, execResult, ph) {
            console.log('Phantom messageAction writing user a message');
            page.evaluate(function (messageText) {
                // определяем фцнкцию клика, чтобы тыкать
                function click(el) {
                    var ev = document.createEvent('MouseEvent');
                    ev.initMouseEvent('click', true, true, window, null, 0, 0, 0, 0, false, false, false, false, 0, null);
                    el.dispatchEvent(ev);
                }

                var textField = document.querySelector('.textfield[name="message"]');
                var buttons = document.querySelectorAll('.button');
                var addButton;
                for (var i = 0; i < buttons.length; i++) {
                    if (buttons[i].value === 'Отправить') {
                        addButton = buttons[i];
                        break;
                    }
                }
                if (!addButton) return 'No submit button found in dom';
                if (!textField) return 'No textfield found in dom';

                textField.value = messageText;

                click(addButton);
            }, function (result) {
                if (result) {
                    ph.exit();
                    return deferred.reject(result);
                }
                // Is meant to go to final method in query, that returns promise
                page.reload();
            }, params.messageText);
        }
    ]
};


module.exports.locationApproveAction = {
    queue: [
        loginMethod,
        locationApproveMethod
    ]
};
