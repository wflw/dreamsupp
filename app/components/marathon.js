var q = require('q'),

    env = require('../../env'),
    appConfig = require('../../config/' + env + '/app'),

    User = require('../models/user'),
    Course = require('../models/course'),
    Checklist = require('../models/checklist'),
    Program = require('../models/program');

function getFromProgram(user, course, prevDay, courseKey, newDayNumber) {
    Program.findOne({_id: course.course.program})
        .execQ()
        .then(function (program) {
            var programDays = program.days;
            console.log('newDayNumber = ' + newDayNumber);
            //console.log(programDays);
            //return;
            for (var key = 0; key < programDays.length; key++) {
                if (programDays[key].dayNumber === newDayNumber) {
                    var chl = {
                        user: user._id,
                        course: course.course,
                        dayNumber: newDayNumber,
                        list: {
                            mandatory: [],
                            pinned: [],
                            simple: []
                        }
                    };
                    // Здесь все происходит через length
                    // потому как это объект с кучей свойств

                    if (programDays[key].checklist) {
                        for (var i = 0; i < programDays[key].checklist.length; i++) {
                            chl.list.mandatory.push({text: programDays[key].checklist[i]});
                        }
                    }
                    if (prevDay.checklist && prevDay.checklist.list.pinned) {
                        for (var j = 0; j < prevDay.checklist.list.pinned.length; j++) {

                            var check = prevDay.checklist.list.pinned[j];
                            chl.list.pinned.push({
                                text: check.text,
                                isDone: false
                            });
                        }
                    }

                    addUserDay(user, chl, newDayNumber, programDays[key], courseKey);
                    break;
                }
            }
        }).catch(function (err) {
            console.log(err);
        });
}

/**
 * Copypast from crons/new-day.js
 * @param userModel
 * @param checklist
 * @param newDay
 * @param quest
 * @param courseKey
 */
function addUserDay(userModel, checklist, newDay, programDay, courseKey) {
    var checklistModel = new Checklist(checklist);
    checklistModel
        .saveQ()
        .spread(function (checklist) {
            console.log('New checklist created with _id = ' + checklist._id);
            userModel.courses[courseKey].days.push({
                dayNumber: newDay,
                title: programDay.title,
                description: programDay.description,
                quest: programDay.quest,
                questForMyself: programDay.questForMyself,
                successDiary: programDay.successDiary,
                feedback: programDay.feedback,
                checklist: checklist._id
            });
            console.log('Now user has ' + userModel.courses[courseKey].days.length + ' days');
            userModel.saveQ()
                .spread(function (u) {
                    console.log('added new day for user with _id = ' + u._id);
                }).catch(function (err) {
                    console.log(err);
                });
        })
        .catch(function (err) {
            console.log(err);
        });
}


module.exports.addUserDay = function (userId, courseId) {
    var defer = q.defer();

    User
        .findOne({_id: userId})
        .populate('courses.days.checklist courses.course')
        .execQ()
        .then(function (user) {
            if (!user)  return defer.reject(new Error('No user found by id'));

            var found = false;
            for (var i = 0; i < user.courses.length; i++) {
                var course = user.courses[i];
                if (String(course.course._id) === String(courseId)) {
                    console.log('Course found');

                    //Check if last three reports are checked
                    if (course.days.length > 2) {
                        var accepted = false;
                        for (var k = course.days.length - 1; k > course.days.length - 1 - appConfig.daysWithoutApprovedReports; k--) {
                            console.log(k);
                            if (course.days[k].isAccepted) {
                                accepted = true;
                            }
                        }
                        if (!accepted) {
                            return defer.reject(new Error('У пользователя не сдано три или более отчетов'));
                        }
                    }

                    var prevDay = course.days[course.days.length - 1];
                    prevDay = prevDay ? prevDay : {dayNumber: 0};
                    var newDayNumber = prevDay.dayNumber + 1;
                    if (newDayNumber > 28) {
                        return defer.reject(new Error('No more days available'));
                    }
                    getFromProgram(user, course, prevDay, i, newDayNumber);

                    found = true;
                }
            }

            if (!found) {
                return defer.reject(new Error('No course with such id found'));
            }

            return defer.resolve();

        })
        .catch(function (err) {
            return defer.reject(new Error('No course with such id found'));
        });

    return defer.promise;
};


module.exports.removeUserDay = function (userId, courseId) {
    var defer = q.defer();

    User
        .findOne({_id: userId})
        .execQ()
        .then(function (user) {
            if (!user) return defer.reject(new Error('No user found by id'));

            var found = false;
            for (var i = 0; i < user.courses.length; i++) {
                var course = user.courses[i];
                if (String(course.course) === String(courseId)) {
                    console.log('Course found');
                    if (course.days.length < 1) {
                        return defer.reject(new Error('There are no more days left'));
                    }
                    course.days.pop();
                    console.log('Now user has ' + course.days.length + ' days');
                    user.saveQ()
                        .spread(function (u) {
                            return defer.reject(new Error('removed day for user with _id = ' + u._id));
                        });

                    found = true;
                }
            }

            if (!found) {
                return defer.reject(new Error('No course with such id found'));
            }
            return defer.resolve();

        })
        .catch(function (err) {
            return defer.reject(new Error(err.message));
        });

    return defer.promise;
};



