module.exports = {
    'featuredList': function(perPage)
    {
        return {
            'collection': 'users',
            'list': {
                'filter': {'courses.isFeatured': true},
                'display': {
                    'profile.firstName':1,
                    'profile.lastName':1,
                    'profile.avatarUrl':1,
                    'profile.timeZone':1,
                    /*'socialProfiles.networkName':1,
                    'socialProfiles.profileUrl':1, */ //Не будем кормить троллей профилями
                    'courses.course':1,
                    'courses.finalReport':1,
                    'courses': {$elemMatch: {'isFeatured': true}},
                    'courses.days':{$slice:[0, perPage]},
                    'courses.days.dayNumber':1,
                    'courses.days.percentDone':1,
                    'courses.days.report':1,
                    'courses.days.reportLink':1
                },
                'courseDisplay':
                {
                    'program': 1
                }
            }
        };
    }
};
