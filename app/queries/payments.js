var mongoose = require('mongoose/lib/');
module.exports = {
    payments: function (userId) {
        var userOId = userId;
        if (!userOId.isValid) {
            userOId = mongoose.Types.ObjectId(userId);
        }
        return {
            collection: 'payments',
            getForCourse: function (courseId) {
                var courseOId = courseId;
                if (!courseOId.isValid) {
                    courseOId = mongoose.Types.ObjectId(courseId);
                }
                return {
                    filter: {
                        user: userOId, //Платеж нашего пользователя
                        course: courseOId, //За конкретный курс
                        days: {$size: 0}, //Оплата за весь курс, а не за конкретный день
                        closed: {$exists: false} //Открытая платежка
                    },
                    display: {'_id': 1}//Нас интересует только идентификатор платежки
                };
            }
        };
    }
};