var mongoose = require('mongoose/lib/');

module.exports = {
    list: function (perPage) {
        return {
            collection: 'users',
            days: function (userId, courseId, page) {
                var courseOId = courseId;
                if (!courseOId.isValid) {
                    courseOId = mongoose.Types.ObjectId(courseId);
                }
                var userOId = userId;
                if (!userOId.isValid) {
                    userOId = mongoose.Types.ObjectId(userId);
                }
                var skip = (page - 1) * perPage;
                return {
                    filter: {
                        '_id': userOId,
                        'courses.course': courseOId
                    },
                    display: {
                        'courses.days.dayNumber': 1,
                        'courses.days.percentDone': 1,
                        'courses.days.report': 1,
                        'courses.days.reportLink': 1,
                        'courses.days.isAccepted': 1,
                        'courses.days': {$slice: [skip, perPage]},
                        'courses': {$elemMatch: {'course': courseOId}}
                    }
                };
            }
        };
    },
    nearest: function (programId) {
        var programOId = programId;
        if (!programOId.isValid) {
            programOId = mongoose.Types.ObjectId(programId);
        }
        return {
            collection: 'courses',
            filter: {
                'isStarted': false,
                'program': programOId
            },
            display: {
                'dateStart': 1,
                'members.user': 1,
                'memberCount': 1,
                'program': 1
            },
            sort: {
                'dateStart': 1,
                'memberCount': -1
            },
            userDisplay: {
                'profile.avatarUrl': 1,
                'profile.firstName': 1,
                'profile.lastName': 1,
                'socialProfiles.networkName': 1,
                'socialProfiles.profileUrl': 1
            },
            programDisplay: {
                'fullCost.currency': 1,
                'fullCost.amount': 1,
                'imageUrl': 1,
                'title': 1
            }
        };
    }
};
