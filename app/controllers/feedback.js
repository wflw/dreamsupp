var _ = require('underscore'),
    moment = require('moment'),

    ObjectId = require('mongoose/lib/').Types.ObjectId,
    User = require('../models/user'),
    Course = require('../models/course'),
    Feedback = require('../models/feedback');

module.exports = function (routes) {
    routes.feedback = {};

    routes.feedback.create = function (req, res) {
        req.checkBody('courseId', 'Курс должен быть указан').isMongoId();

        var errors = req.validationErrors();
        if (errors) return res.json({success: false, errorMessages: errors});

        var feedback = {
            user: req.userId,
            course: req.body.courseId,
            items: req.body.items
        };
        (new Feedback(feedback))
            .saveQ()
            .spread(function (feedback) {
                return res.json({success: true, data: {_id: feedback._id}});
            }).catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err.message]});
            });
    };
};