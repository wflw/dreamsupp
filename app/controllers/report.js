var moment = require('moment'),
    _ = require('underscore'),
    q = require('q'),
    ObjectId = require('mongoose/lib/').Types.ObjectId,

    User = require('../models/user'),
    Program = require('../models/program'),
    CheckList = require('../models/checklist');

module.exports = function (routes, events) {
    routes.report = {};

    routes.report.notVerified = function (req, res) {
        return User.find({'courses.days': {
            '$elemMatch':{
                'report': {$ne: ''},
                'isAccepted': {$exists: false},
                'needToBeChecked': true
            }
        }}, {
            'profile.firstName': 1,
            'profile.lastName': 1,
            'profile.avatarUrl': 1,
            'profile.timeZone': 1,
            'socialProfiles.profileUrl': 1,
            'courses.course': 1,
            'courses._id': 1,
            'courses.days._id':1,
            'courses.days.dayNumber': 1,
            'courses.days.percentDone': 1,
            'courses.days.report': 1,
            'courses.days.comments': 1,
            'courses.days.needToBeChecked':1,
            'courses.days.isAccepted':1
        })
            .sort({
                'courses.days.comments.date': 1 //Сортируем по свежести комментариев
            }).populate({
                path: 'courses.course',
                select: {
                    'program': 1,
                    'dateStart': 1,
                    'dateEnd': 1,
                    'socialGroups':1
                }
            }).populate({
                path: 'courses.days.comments.moderator',
                select: {
                    'profile.firstName':1,
                    'profile.lastName':1,
                    'profile.avatarUrl':1,
                    'socialProfiles': {$slice : 1}
                }
            })
            .execQ()
            .then(function (users) {
                return Program.populateQ(users, {
                    path: 'courses.course.program',
                    select: {
                        'title':1
                    }
                });
            }).then(function(users){
                var daysToDelete = [];
                var coursesToDelete = [];
                for (var i=0; i<users.length; i++){
                    var user = users[i];
                    for (var j = 0; j< user.courses.length; j++){
                        var course = user.courses[j];
                        var saveCourse = course.days.length;
                        for (var k = 0; k<course.days.length; k++ ){
                            var day = course.days[k];
                            if (!day.report || day.isAccepted|| !day.needToBeChecked){
                                daysToDelete.push(day);
                                saveCourse--;
                            }
                        }
                        if (!saveCourse){
                            coursesToDelete.push(course);
                        }
                    }
                }
                for (i=0; i<daysToDelete.length; i++){
                    daysToDelete[i].remove();
                }
                for (i=0; i<coursesToDelete.length; i++){
                    coursesToDelete[i].remove();
                }

                return res.json(
                    {
                        success: true,
                        data: users
                    }
                );
            }).catch(function (reason) {
                console.log(reason);
                return res.json({
                    success: false,
                    errorMessages: [reason]
                });
            });
    };

    routes.report.respond = function (req, res) {
        /*Правила валидации смотреть :
         https://github.com/chriso/validator.js
         https://github.com/ctavan/express-validator
         */
        var isCoach = req.userRole === 'coach';

        req.checkBody('courseId', 'Курс должен быть указан').isMongoId();
        if (isCoach) { //В случае тренера у нас обязательно должен быть указан пользователь с которым мы общаемся
            req.checkBody('userId', 'Идентификатор пользователя указан неверно').isMongoId();
            req.checkBody('verb', 'Действие должно быть в диапазоне [\'a\',\'m\',\'r\']').isIn(['a', 'm', 'r']);
        } else {
            req.checkBody('verb', 'Действие может быть только \'m\' для участника').equals('m');
        }
        req.checkBody('dayNumber', 'Должен быть указан номер дня').isInt();


        var courseId = req.body.courseId,
            userId = isCoach ? req.body.userId : req.userId,
            authorId = isCoach ? req.userId : null,
            dayNumber = req.body.dayNumber,
            message = req.body.message,
            verb = req.body.verb; //что это у нас, просто сообщение, подтверждение или отказ

        if (verb === 'm') {
            req.checkBody('message', 'Должно быть указано сообщение').notEmpty();
        }

        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }

        /*Если это коуч, то мы ищем нужный день и метим день, как провереный
         или если отказ, то снимаем метку необходимости проверки
         если просто сообщение, только добавляем сообщение
         */

        var updateCommand = q.defer();

        var update = {$set: {}},
            dayPath = 'courses.$.days.' + (+dayNumber - 1); //TODO:ДНИ ДОЛЖНЫ ИДТИ ПО ПОРЯДКУ, В ПРОТИВНОМ СЛУЧАЕ ВСЕ ПРАХОМ!
        if (message) {//Добавляем сообщение, если оно необходимо
            update.$push = {};

            var newMessage = {
                text: message,
                verb: verb,
                date: moment().format()
            };
            if (authorId) {
                newMessage.moderator = authorId;
            }

            update.$push[dayPath + '.comments'] = newMessage;
        }

        var eventName;

        if (isCoach) {
            switch(verb){
                case 'm':
                    eventName = 'reportMessage';
                    delete update.$set; //Ну нечего сетить в таком случае, только сообщение отправить
                    break;
                case 'r':
                    eventName = 'reportRejected';
                    break;
                case 'a':
                    eventName = 'reportAccepted';
                    break;
            }
            if (verb !== 'm') {
                //Если не простое сообщение от модератора, то снимаем флаг необходимости проверки
                update.$set[dayPath + '.needToBeChecked'] = false;
            }
            if (verb === 'a') {
                //Если это аппрув пропускаем отчет
                update.$set[dayPath + '.isAccepted'] = true;
                //А еще считаем сколько галочек начекала наша красавица
                CheckList.aggregate({
                    '$match': {
                        'user': ObjectId(userId),
                        'course': ObjectId(courseId),
                        'dayNumber': dayNumber
                    }
                },{
                    '$project': {
                        '_id': 0,
                        'allChecks': {
                            '$setUnion':['$list.mandatory', '$list.pinned', '$list.simple']
                        }
                    }
                },{
                    '$unwind': '$allChecks'
                },{
                    '$group': {
                        '_id': '$allChecks.isDone',
                        'count': {'$sum': 1}
                    }
                },{
                    '$sort': {'_id': -1} //Чтобы true были 0-ым элементом, а false 1-ым
                }).execQ()
                    .then(function(checkCounts){
                        var percent = 100;
                        if (checkCounts.length>0){
                            if (checkCounts.length === 1){
                                if (!checkCounts[0]._id){//False, ничего не сделали
                                    percent = 0;
                                }
                            } else {
                                var checked = checkCounts[0].count,
                                    skipped = checkCounts[1].count,
                                    total = checked+skipped;
                                percent = Number(((checked/total)*100).toFixed(1));
                            }
                        }
                        update.$set[dayPath + '.percentDone'] = percent;
                        updateCommand.resolve(update);
                    }).catch(function(reason){
                        updateCommand.reject(reason);
                    });
            } else {
                updateCommand.resolve(update);
            }
        } else {
            //Если мы сообщаем что-то, то это флаг на проверку
            update.$set[dayPath + '.needToBeChecked'] = true;
            updateCommand.resolve(update);
        }

        return updateCommand.promise.then(function(updatePart){
            return User.updateQ({
                    '_id': ObjectId(userId),
                    'courses': {
                        '$elemMatch': {
                            'course': ObjectId(courseId),
                            'days.dayNumber': dayNumber
                        }
                    }
                }, updatePart
            ).then(function () {
                    if (eventName){
                        events.emit(eventName, userId, courseId, dayNumber, message);
                    }
                    return res.json({
                        success: true,
                        data: 'Сообщение отправлено'
                    });
                });
        }).catch(function (reason) {
            console.log(reason);
            //console.log('lolol');
            return res.json({
                success: false,
                errorMessages: [reason]
            });
        });
    };

    /**
     *
     * @param req courseId, dayNumber, report
     * @param res
     * @returns {*}
     */
    routes.report.save = function (req, res) {
        req.checkBody('courseId', 'Некорректный идентификатор курса').isMongoId();
        req.checkBody('dayNumber', 'Некорректный номер дня').isInt();
        req.checkBody('report', 'Некорректный отчет').isLength(1);

        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }

        User.findOne({_id: req.userId})
            .execQ()
            .then(function (user) {
                if (!user) {
                    return res.json({success: false, errorMessages: ['Не найден пользователь с таким _id']});
                }

                var courseFound = false;
                for (var i = 0; i < user.courses.length; i++) {
                    if (String(user.courses[i].course) === String(req.param('courseId'))) {
                        courseFound = true;
                        var days = user.courses[i].days;
                        var dayFound = false;
                        for (var j = 0; j < days.length; j++) {
                            var day = days[j];
                            if (day.dayNumber === req.param('dayNumber')) {
                                dayFound = true;
                                day.report = req.param('report');
                            }
                        }
                        if (!dayFound) {
                            return res.json({success: false, errorMessages: ['У пользователя нет этого дня в курсе']});
                        }
                    }
                }
                if (!courseFound) {
                    return res.json({success: false, errorMessages: ['У пользователя нет курса с таким _id']});
                }

                user.saveQ().spread(function (user) {
                    return res.json({success: true, data: {_id: user._id}});

                });

            })
            .catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err.message]});
            });
    };

    var setCheckReportNeeded = function(userId, courseId, dayNumber, checkState){
        return User.findOne({_id: userId})
            .execQ()
            .then(function (user) {
                if (!user) {
                    throw 'Не найден пользователь с таким _id';
                }
                var courseFound = false;
                for (var i = 0; i < user.courses.length; i++) {
                    if (String(user.courses[i].course) === String(courseId)) {
                        courseFound = true;
                        var days = user.courses[i].days;
                        var dayFound = false;
                        for (var j = 0; j < days.length; j++) {
                            var day = days[j];
                            if (day.dayNumber === dayNumber) {
                                dayFound = true;
                                day.needToBeChecked = checkState;
                            }
                        }
                        if (!dayFound) {
                            throw  'У пользователя нет этого дня в курсе';
                        }
                    }
                }
                if (!courseFound) {
                    throw 'У пользователя нет курса с таким _id';
                }

                return user.saveQ().spread(function (user) {
                    return user._id;
                });
            });
    };

    routes.report.markCheckNeeded = function (req, res) {
        req.checkBody('courseId', 'Курс должен быть указан').isMongoId();
        req.checkBody('dayNumber', 'День должен быть указан').isInt();
        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }
        return setCheckReportNeeded(req.userId, req.param('courseId'), req.param('dayNumber'), true)
            .then(function(userId){
                return res.json({success: true, data: {_id: userId}});
            }).catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err.message]});
            });
    };

    routes.report.markCheckNotNeeded = function (req, res) {
        req.checkBody('courseId', 'Курс должен быть указан').isMongoId();
        req.checkBody('dayNumber', 'День должен быть указан').isInt();
        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }
        return setCheckReportNeeded(req.userId, req.param('courseId'), req.param('dayNumber'), false)
            .then(function(userId){
                return res.json({success: true, data: {_id: userId}});
            }).catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err.message]});
            });
    };

};