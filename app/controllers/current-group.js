var moment = require('moment'),
    q = require('q'),
    ObjectId = require('mongoose/lib/').Types.ObjectId,
    _ = require('underscore'),

    User = require('../models/user'),
    Program = require('../models/program'),
    Course = require('../models/course'),

    repo = require('../components/courses');

module.exports = function (routes) {

    var setCourseCurrentDayAndFinished = function (course, timeZone) {
        course.isFinished = moment().isAfter( //Уже закончился?
            moment(course.dateEnd)
        );
        if (course.days.length > 0) {
            course.currentDay = _.first(course.days).dayNumber; //Текущий день марафона, выданный пользователю
        }
        course.daysFromStart = 1 + moment() //Какой день марафона идет со старта марафона
            .add(timeZone, 'h')
            .diff(course.dateStart, 'days');
    };

    var populateProgramsAndMembers = function (courseQuery) {
        return courseQuery.populate({
            path: 'program', //Еще нужно затянуть учебную программу
            select: {
                title: 1, //Заголовок
                imageUrl: 1, //Иконка
                description: 1 //Описание
            }
        }).populate({
            path: 'members.user', //Еще нужны профили участников
            select: {
                'profile.firstName': 1, //Имя
                'profile.avatarUrl': 1, //Автврка
                'socialProfiles.networkName': 1, //Имя соцести
                'socialProfiles.profileUrl': 1 //Ссылка на профиль в соцсети
            }
        });
    };

    var getCourseByIds = function (courseIds) {
        return populateProgramsAndMembers(Course.findOne({
            _id: {$in: courseIds},
            isStarted: false //Еще не начатый курс
        }, {
            dateStart: 1, //Дата начала марафона
            dateEnd: 1, //Дата конца марафона
            program: 1, //Учебная программа
            members: 1 // Берем участников программы
        })).sort({
            'members.dateJoined': -1 //Нам нужно будет впоследствии разделить на 2 части оплативших и желающих
        })
        .execQ();
    };

    var getLastOpenCourse = function () {
        return populateProgramsAndMembers(Course.findOne({
            isStarted: false //Еще не начатый курс
        }, {
            dateStart: 1, //Дата начала марафона
            dateEnd: 1, //Дата конца марафона
            program: 1, //Учебная программа
            members: 1 //Берем участников программы
        }))
            .sort({
                dateStart: 1, //Даты по возрастанию, т.е. ближайшая из незакрытых
                'members.dateJoined': 1 //Нам нужно будет впоследствии разделить на 2 части оплативших и желающих
            }).execQ();
    };

    var getUserWithCourses = function (userId) {
        return User.findOne({
            '_id': ObjectId(userId)
        }, {
            'profile.firstName': 1,
            'profile.lastName': 1,
            'profile.avatarUrl': 1,
            'profile.age': 1,
            'profile.city': 1,
            'profile.timeZone': 1,
            'courses.dateJoined': 1,
            'courses.dateExited': 1,
            'courses.course': 1,
            'courses.days.dayNumber': 1,
            'courses.days': {
                '$slice': -1
            },
            'courses': 1
        }).sort({
            'dateExited': 1,
            'dateJoined': -1
        }).populate({
            path: 'courses.course',
            select: {
                'dateStart': 1,
                'dateEnd': 1,
                'isStarted': 1
            }
        }).execQ();
    };

    var getGroupResult = function (course, user) {
        var result = {};
        if (course) {
            var ds = moment(course.dateStart),
                now = moment(),
                diff = moment(ds.diff(now));
            result = course.toJSON();

            //Определяем время до проведения мероприятия
            result.eventTime = ds.format('X');
            result.timeToEvent =
            {
                days: ds.diff(now, 'days'),
                hours: diff.hours(),
                minutes: diff.minutes(),
                seconds: diff.seconds()
            };

            //Нужно разделить желающих и участников

            result.members = _.groupBy(result.members, function (member) {
                return member.dateJoined ? 'active' : 'waiting';
            });
        }


        if (user) {
            if (course) {
                //Надо посмотреть, а текущий курс вообще есть в актуальных у нашего пользователя
                var matchCourse = _.find(user.courses, function (crs) {
                    return crs.course._id.equals(course._id);
                });
                if (!matchCourse) {
                    user.status = 'not-in'; //Не в группе, предлагаем вступить
                } else if (matchCourse.dateJoined) {
                    user.status = 'active'; //Заплатил и готов к действиям
                } else {
                    user.status = 'waiting'; //Ждет вступления/оплаты
                }
            } else {
                user.status = 'free'; //Нет доступных для встпуления курсов
            }

            _.each(
                _.where(
                    _.map(user.courses, function (course) {
                        var innerCourse = course.course;
                        delete course.course;
                        course.dateStart = innerCourse.dateStart;
                        course.dateEnd = innerCourse.dateEnd;
                        course.isStarted = innerCourse.isStarted;
                        course.course = innerCourse._id;
                        return course;
                    }), {isStarted: true}),
                function (course) {
                    setCourseCurrentDayAndFinished(course, user.profile.timeZone);
                }
            );
            result.user = user;
        }
        return result;
    };

    routes.groups = {
        active: function (req, res) {
            var userCourseFound = q.defer(),
                courseFound,
                currentUser;
            if (req.userId) {
                //Если пользователь залогинен, значит он уже может быть в открытом курсе
                currentUser = getUserWithCourses(req.userId)
                    .then(function (user) {
                        if (user) {
                            var jUser = user.toJSON();
                            var sourceCourses = jUser.courses;
                            jUser.courses = _.filter(sourceCourses, function(course){ return !course.dateExited; });
                            jUser.pastCourses = _.filter(sourceCourses, function(course){ return course.dateExited; });
                            if (jUser.courses && jUser.courses.length > 0) {
                                userCourseFound.resolve(getCourseByIds(
                                        _.map(
                                            jUser.courses,
                                            function (item) {
                                                return item.course;
                                            }))
                                );
                            } else {
                                userCourseFound.resolve(null);
                            }
                            return jUser;
                        } else {
                            userCourseFound.reject('incorrect user credentials, relogin please');
                            return null;
                        }
                    });
            }

            if (!currentUser) { //Не залогинены
                courseFound = getLastOpenCourse();
            } else { //Залогинены, но курса может и не быть у пользователя
                courseFound = userCourseFound
                    .promise
                    .then(function (course) {
                        var retCourse = course;
                        if (!course) {
                            retCourse = getLastOpenCourse();
                        }
                        return retCourse;
                    });
            }

            return courseFound.then(function (course) {
                if (currentUser) {
                    return currentUser.then(function (user) {
                        return res.json({
                            success: true,
                            data: getGroupResult(course, user)
                        });
                    });
                } else {
                    return res.json({
                        success: true,
                        data: getGroupResult(course)
                    });
                }
            }).catch(function (reason) {
                console.log(reason);
                return res.json({
                    success: false,
                    errorMessages: [reason]
                });
            });
        },
        join: function (req, res) {
            return repo.addUserToCourseQ(req.userId, req.params.courseId)
                .then(function () {
                    return res.json(
                        {
                            success: true,
                            data: {}
                        }
                    );
                })
                .catch(function (reason) {
                    return res.json(
                        {
                            success: false,
                            errorMessages: [reason]
                        }
                    );
                });
        },
        my: function (req, res) {
            //Мои группы. Те группы в которых учавствовал пользователь
            return repo.getUserActiveCoursesQ(req.userId)
                .then(function (user) {
                    return res.json(
                        {
                            success: true,
                            data: user.courses
                        }
                    );
                })
                .catch(function (reason) {
                    return res.json(
                        {
                            success: false,
                            errorMessages: [reason]
                        }
                    );
                });
        },
        membersProgress: function (req, res) {

            return User.findOne({
                '_id': ObjectId(req.userId),
                'courses.dateJoined': {$exists: true},
                'courses.course': ObjectId(req.params.courseId)
            }, {
                'courses.course': 1,
                'courses.days.dayNumber': 1,
                'courses.days': {
                    '$slice': -1
                },
                'courses': {
                    '$elemMatch': {
                        'course': ObjectId(req.params.courseId)
                    }
                }
            }).populate({
                path: 'courses.course',
                select: {
                    'isStarted': 1,
                    'memberCount': 1,
                    'socialGroups': 1,
                    'dateStart': 1,
                    'dateEnd': 1,
                    'program': 1
                }
            }).execQ()
                .then(function (foundUser) {
                    if (!foundUser) {
                        throw 'Current user not associated with requested group';
                    }
                    return Program.populate(foundUser, {
                        path: 'courses.course.program',
                        select: {
                            'title': 1,
                            'imageUrl': 1,
                            'description': 1
                        }
                    })
                        .then(function () {
                            return User.find({
                                'courses.dateJoined': {$exists: true},
                                'courses.course': ObjectId(req.params.courseId)
                            }).select({
                                'profile.firstName': 1,
                                'profile.lastName': 1,
                                'profile.avatarUrl': 1,
                                'profile.timeZone': 1,
                                'socialProfiles.profileUrl': 1,
                                'courses.finalReport': 1,
                                'courses.dateExited': 1,
                                'courses.days.dayNumber': 1,
                                'courses.days.percentDone': 1,
                                'courses.days.report': 1,
                                'courses.days.reportLink': 1,
                                'courses.days.isAccepted': 1,
                                'courses': {
                                    '$elemMatch': {
                                        'course': ObjectId(req.params.courseId)
                                    }
                                },
                                'courses.days': {
                                    $slice: [0, +req.params.perPage]
                                }
                            }).sort({
                                'courses.dateExited': 1,
                                'courses.days.isAccepted': 1,
                                'courses.days.dayNumber': 1
                            }).execQ()
                                .then(function (users) {
                                    return {
                                        timeZone: foundUser.profile.timeZone,
                                        users: users,
                                        course: foundUser.courses[0].toJSON() //Если нашлось, то это будет 100% один курс и он 0-ой индекс
                                    };
                                });
                        });
                })
                .then(function (reslt) {
                    reslt.course.course.days = reslt.course.days;
                    setCourseCurrentDayAndFinished(reslt.course.course, reslt.timeZone);
                    var result = {
                        course: reslt.course.course,
                        users: []
                    };
                    var currentUserId = ObjectId(req.userId);
                    _.forEach(reslt.users, function (user) {
                        var usr = user.toJSON(),
                            course = user.courses[0];
                        delete usr.courses;
                        course.days = _.reject(course.days, function (day) {
                            return !day.isAccepted;
                        });
                        usr.course = course;
                        if (usr._id.equals(currentUserId)) { //Смотрим это мы или нет, если мы, то гаранитруем что мы в начале и со специальной меткой
                            usr.isItMe = true;
                            result.users.unshift(usr);
                        } else {
                            result.users.push(usr);
                        }
                    });
                    return res.json(
                        {
                            success: true,
                            data: result
                        });
                }).catch(function (reason) {
                    console.log(reason);
                    return res.json(
                        {
                            success: false,
                            errorMessages: [reason]
                        }
                    );
                });
        }
    };
};