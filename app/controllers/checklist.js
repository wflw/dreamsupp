var Checklist = require('../models/checklist');

module.exports = function (routes) {
    routes.checklist = {};

    routes.checklist.update = function (req, res) {
        // Проверяем свой ли чеклист собрался редактировать юзер
        if (String(req.userId) !== String(req.body.user)) {
            return res.status(401).json({
                success: false,
                errorMessages: ['Вы не можете редактировать челисты другого пользователя']
            });
        }

        Checklist.findOne({_id: req.params.checklistId})
            .execQ()
            .then(function (chl) {
                if (!chl) {
                    console.log('No checklist found by _id = ' + req.params.checklistId);
                    return res.json({success: false, errorMessages: ['По данному id не найдено чеклистов']});
                }

                chl.list = req.body.list;
                chl.saveQ()
                    .spread(function (updatedChl) {
                        return res.json({success: true, data: updatedChl});

                    }).catch(function (err) {
                        console.log(err);
                        return res.json({success: false, errorMessages: [err.message]});
                    });

            })
            .catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err.message]});
            });

    };
};