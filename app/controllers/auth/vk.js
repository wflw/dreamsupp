var jwt = require('jsonwebtoken'),
    VK = require('vkapi'),
    request = require('request'),
    _ = require('underscore'),
    moment = require('moment'),

    ObjectId = require('mongoose/lib/').Types.ObjectId,

    authConfig = require('../../../config/' + app.get('env') + '/auth'),

    User = require('../../../app/models/user.js'),
    coursesRepo = require('../../../app/components/courses');

var generateRedirectUrl = function (user, token) {
    var data = {
        token: token,
        _id: user._id,
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        avatarUrl: user.profile.avatarUrl,
        timeZone: user.profile.timeZone,
        role: user.role
    };
    return '/#/token?json=' + encodeURIComponent(JSON.stringify(data));
};

var getRedirectUrl = function (user) {
    var token = jwt.sign({_id: user._id}, authConfig.secretToken, {expiresInMinutes: 60});
    return generateRedirectUrl(user, token);
};

module.exports = function (routes, events) {
    routes.auth = routes.auth || {};
    routes.auth.vk = {};

    routes.auth.vk.start = function (req, res) {
        var localPattern = /dreamsupp.local/gi;
        if (localPattern.test(req.get('Referrer'))) {
            User.findOne({
                'socialProfiles.networkName': 'vk',
                'socialProfiles.profileId': '125112070',
                'profile.firstName': 'Jax',
                'profile.lastName': 'Teller'
                }, {
                    'role':1,
                    'profile':1,
                    'courses.course':1,
                    'courses.dateExited':1
                })
                .execQ()
                .then(function (userFound) {
                    var addPromise;
                    if (req.params.courseId) {//Если есть курс, зачисляем в него
                        var targetCourse = _.find(userFound.courses, function (course) { //но для начала надо проверить, а не вышвырнули ли его
                            return course.course.equals(ObjectId(req.params.courseId)) && course.dateExited;
                        });
                        if (targetCourse) {
                            throw 'user rejected from course';
                        }
                        addPromise = coursesRepo.addUserToCourseQ(userFound._id, req.params.courseId);
                    }
                    if (userFound) {
                        if (addPromise) {
                            return addPromise.then(function () {
                                events.emit('userAddedToCourse', userFound._id, ObjectId(req.params.courseId));
                                return getRedirectUrl(userFound);
                            });
                        } else {
                            return getRedirectUrl(userFound);
                        }
                    } else {
                        return '/';
                    }
                })
                .then(function (redirectUrl) {
                    res.redirect(redirectUrl);
                })
                .catch(function (err) {
                    console.log(err);
                    res.redirect('/#/login?error=' + encodeURIComponent(err));
                })
                .done();

        } else {
            res.redirect(authConfig.vk.getOauth1(req.params.courseId));
        }
    };

    routes.auth.vk.callback = function (req, resUp) {
        if (req.query.code) {
            request(authConfig.vk.getOauth2(req.query.code), function (error, response, body) {
                if (!error) {
                    var vkRes = JSON.parse(body);
                    if (response.statusCode !== 200) {
                        console.log(response);
                        resUp.redirect('/#/login?error=code_expired');
                        return;
                    }

                    var vk = new VK({'mode': 'oauth'});
                    vk.setToken({token: vkRes.access_token});

                    var params = {
                        'user_ids': vkRes.user_id,
                        'fields': [
                            'photo_max',
                            'sex',
                            'screen_name',
                            'timezone',
                            'bdate',
                            'city',
                            'country'
                        ],
                        'name_case': 'nom'
                    };

                    vk.request('users.get', params);
                    vk.on('done:users.get', function (resIn) {
                        var profile = resIn.response[0];

                        // Проверка на пол пользователя
                        if (+profile.sex === 2) {
                            return resUp.redirect('/#/wrongGender');
                        }

                        var bdatePattern = /\d{1,2}\.\d{1,2}\.\d{4}/gi,
                            userAge;

                        if (bdatePattern.test(profile.bdate)){
                            userAge = moment().diff(moment(profile.bdate, 'D.M.YYYY'),'years');
                        }

                        User.findOne({
                            'socialProfiles.networkName': 'vk',
                            'socialProfiles.profileId': profile.uid
                        }, {
                            'role':1,
                            'profile':1,
                            'courses.course':1,
                            'courses.dateExited':1
                        })
                            .sort({'courses.dateJoined': -1})
                            .execQ()
                            .then(function (userFound) {
                                if (userFound) {
                                    if (req.query.state) {
                                        //Регистрация на курс.
                                        //Надо создать счет за идентификатором пользователя.
                                        //При успешном создании счета осуществляется переход на результат входа.

                                        var targetCourse = _.find(userFound.courses, function (course) { //но для начала надо проверить, а не вышвырнули ли его
                                            return course.course.equals(ObjectId(req.query.state)) && course.dateExited;
                                        });
                                        if (targetCourse) {
                                            return '/#/login?error=course_rejected'; //Был снят с курса
                                        }
                                        return coursesRepo.addUserToCourseQ(userFound._id, req.query.state)
                                            .then(function () {
                                                //Асинхронно обновляем профиль
                                                return getRedirectUrl(userFound);
                                            });
                                    }
                                    else {
                                        return getRedirectUrl(userFound);
                                    }
                                } else { //Пользователь не найден, создаем нового
                                    var user = {
                                        role: 'student',
                                        profile: {
                                            firstName: profile.first_name,
                                            lastName: profile.last_name,
                                            avatarUrl: profile.photo_max || '',
                                            timeZone: profile.timezone,
                                            country: profile.country,
                                            city: profile.city,
                                            birthday: profile.bdate
                                        },
                                        socialProfiles: [
                                            {
                                                networkName: 'vk',
                                                profileId: profile.uid,
                                                profileUrl: 'http://vk.com/' + profile.screen_name,
                                                token: vkRes.access_token,
                                                code: req.query.code
                                            }
                                        ]
                                    };
                                    if (userAge){
                                        user.profile.age = userAge;
                                    }

                                    var newUserModel = new User(user);
                                    return newUserModel.saveQ().spread(function (newUser) {
                                        if (req.query.state) {
                                            return coursesRepo.addUserToCourseQ(newUser._id, req.query.state)
                                                .then(function () {
                                                    events.emit('userAddedToCourse', newUser._id, ObjectId(req.query.state));
                                                    return getRedirectUrl(newUser);
                                                });
                                        }
                                        else {
                                            return getRedirectUrl(newUser);
                                        }
                                    });
                                }
                            })
                            .then(function (redirectUrl) {
                                resUp.redirect(redirectUrl);
                            })
                            .catch(function (err) {
                                console.log(err);
                            })
                            .done();
                    });
                } else {
                    console.log(error);
                }
            });
        }
    };
};
