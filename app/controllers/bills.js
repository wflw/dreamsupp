var moment = require('moment'),
    q = require('q'),
    ObjectId = require('mongoose/lib/').Types.ObjectId,
    md5 = require('MD5'),

    User = require('../models/user'),
    Program = require('../models/program'),
    Payment = require('../models/payment'),
    Course = require('../models/course'),

    merchantConfig = require('../../config/' + app.get('env') + '/merchant'),
    appConfig = require('../../config/' + app.get('env') + '/app'),

    queryPayment = require('../queries/payments');

module.exports = function (routes) {

    var tranCookieCheck = function (key, action) {
        if (key) { //Если кука есть, то есть смысл что-то делать
            return action();
        } else {
            //Некорректная кука
            var error = q.defer();
            error.resolve({
                success: false,
                errorMessages: ['incorrect transaction id in cookie']
            });
            return error.promise;
        }
    };

    var findBill = function (tranId) {
        var tran = ObjectId(tranId);
        return Payment.find({
                '_id': tran, // Та, что требуется
                'closed': {$exists: false} //И при этом еще не закрытая
            }, {
                '_id': 1,
                'course': 1,
                'created': 1,
                'user': 1
            }
        ).populate({
                'path': 'course',
                'select': {
                    'program': 1
                }
            })
            .limit(1)
            .execQ();
    };

    var getFullTran = function (tranId) {
        return findBill(tranId)
            .then(populateProgram)
            .then(function (bills) {
                if (bills.length === 0) {
                    throw 'transaction not found';
                } else {
                    var bill = bills[0];
                    return {
                        success: true,
                        data: {
                            id: bill._id,
                            crated: bill.created,
                            imageUrl: bill.course.program.imageUrl,
                            title: bill.course.program.title,
                            cost: {
                                amount: bill.course.program.fullCost[0].amount,
                                currency: bill.course.program.fullCost[0].currency
                            }
                        }
                    };
                }
            })
            .catch(function (reason) {
                return {
                    success: false,
                    errorMessages: [reason]
                };
            });
    };

    var populateProgram = function (payment) {
        return Program.populateQ(payment, {
            'path': 'course.program',
            'select': {
                'fullCost.currency': 1,
                'fullCost.amount': 1,
                'title': 1,
                'imageUrl': 1
            }
        });
    };

    var closeBill = function (tranId, closeReason) {
        /*Надо зафиксировать провал оплаты
         closed,
         transactions.rejected,
         transactions.sent
         transactions.amount
         transactions.currency
         Убрать пользователя из группы
         Выдать информцию о проваленом заказе.
         */
        return findBill(tranId)
            .then(function (payments) {
                //Если патеж не найден, ругаемся
                if (payments.length === 0) {
                    throw 'transaction not found or already closed';
                }
                else {
                    var payment = payments[0];
                    /*
                     Если найден, сразу удаляем нашего участника, если он в группе
                     Это операцию мы отправляем в открытое плавание
                     */
                    Course.updateQ({
                        '_id': payment.course._id, //Ищем курс
                        'members.user': payment.user // С нашим участником
                    }, {
                        '$inc': {'memberCount': -1}, //Снимаем счетчик участников
                        '$pull': {
                            'members': {
                                'user': ObjectId(payment.user) //И самого участника удалим
                            }
                        }
                    });//TODO: Надо будет позднее логгировать исключение из группы и все аналогичные опреации

                    return populateProgram(payment);
                }
            })
            .then(function (payment) {
                /*
                 Выставляем провальность транзакции
                 Это операцию мы отправляем в открытое плавание
                 */

                var tranItem = {
                    'sent': payment.created, //TODO: Эту дату надо брать из того ретрая за которым последовал отказ
                    'amount': payment.course.program.fullCost[0].amount,
                    'currency': payment.course.program.fullCost[0].currency
                };

                tranItem[closeReason] = moment().format();

                return Payment.updateQ({
                    '_id': payment._id
                }, {
                    $set: {
                        'closed': moment().format()
                    },
                    '$addToSet': {
                        'transactions': tranItem
                    }
                }).then(function () {
                    return payment; //Возвращаем в цепочку полностью заполненный платеж
                });
            })
            .then(function (payment) {
                return {
                    success: true,
                    data: {
                        id: payment._id,
                        crated: payment.created,
                        imageUrl: payment.course.program.imageUrl,
                        title: payment.course.program.title,
                        cost: {
                            amount: payment.course.program.fullCost[0].amount,
                            currency: payment.course.program.fullCost[0].currency
                        }
                    }
                };
            })
            .catch(function (reason) {
                return {
                    success: false,
                    errorMessages: [reason]
                };
            });
    };

    routes.bills = {
        'create': function (req, res) {
            var currentProgram;
            /*
             Вписываем пользователя на курс. Если не оплатит, вылетит,
             Смотрим, есть ли уже открытый счет у этого пользователя на заданный курс
             Если есть, то мы пробуем отправить на оплату тот же счет и добавляем ему дату повтора
             Если нет, создаем новый счет на оплату
             Создаем счет для пользователя на конкретный курс
             Вписываем путсой курс пользователю, без копирования содержимого
             Выставляем информацию о транзакции в пустом курсе
             Также учитываем, каково наполнение группы, т.к. группа может быть уже полна и нужно предложить новую (пока не реализуем)
             */

            var userPayment = function () {
                return Payment.find(qry.filter)
                    .select(qry.display)
                    .limit(2) //Чтобы можно было проверить > 1 совпедения
                    .execQ()
                    .then(function (payments) {
                        var retryId;
                        var paymentId = q.defer();
                        if (payments.length > 0) {
                            //Если нашелся платеж, будем работать с ним,
                            var foundPayment = payments[0]._id;
                            if (payments.length > 1) {
                                paymentId.reject('more than one payment for course'); //Или отклоняем промис если более одной платежки
                                paymentId = paymentId.promise;
                            }
                            else {
                                retryId = ObjectId();
                                paymentId = Payment.updateQ({ //Добавляем время повтора для существующего счета
                                    '_id': foundPayment
                                }, {
                                    '$addToSet': {
                                        'retries': {
                                            'id': retryId
                                        }
                                    }
                                }).then(function () {
                                    return {
                                        'paymentId': foundPayment,
                                        'retryId': retryId
                                    };
                                });
                            }
                        } else {
                            //в противном случае, добавляем новый счет
                            var payment = new Payment(
                                {
                                    providerName: 'payanyway', //TODO: В дальнейшем надо будет разделить аггрегаторы в настройках
                                    user: ObjectId(req.userId),
                                    course: ObjectId(req.params.courseId)
                                }
                            );
                            paymentId = payment.saveQ()
                                .then(function (newPayment) {
                                    newPayment = newPayment[0];
                                    //Добавляем информацию о платеже пользователю
                                    return User.updateQ({ //Ищем нашего пользователя с уже открытым курсом, возможно это не первая оплата
                                            '_id': ObjectId(req.userId),
                                            'courses.course': ObjectId(req.params.courseId)
                                        }, {
                                            $addToSet: {
                                                'courses.$.purchaseHistory': { //Добавим очередную оплату за курс
                                                    'payment': newPayment._id
                                                }
                                            }
                                        }
                                    ).spread(function (affectedRows) {
                                            if (affectedRows > 0) {//Был найден курс и к нему было добавлена оплата
                                                return { //Возвращаем в цепочку индентификатор платежа
                                                    'paymentId': newPayment._id,
                                                    'retryId': null
                                                };
                                            }
                                            else {
                                                return User.updateQ({ //Ищем нашего пользователя без открытого нужного курса
                                                    '_id': ObjectId(req.userId),
                                                    'courses.course': {$nin: [ObjectId(req.params.courseId)]}
                                                }, {
                                                    $addToSet: { //Добавляем новый, пустой нужный курс, с оплатой
                                                        'courses': {
                                                            'course': ObjectId(req.params.courseId),
                                                            'purchaseHistory': [
                                                                {
                                                                    'payment': newPayment._id
                                                                }
                                                            ]
                                                        }
                                                    }
                                                }).then(function () {
                                                    return { //Если все успешно добавлено возвращаем в цепочку id платежа
                                                        'paymentId': newPayment._id,
                                                        'retryId': null
                                                    };
                                                });
                                            }
                                        });
                                });
                        }
                        return paymentId; //Возвращаем промис на Id платежа
                    });
            };

            var targetCourse = Course.findOneAndUpdate(
                {
                    '_id': ObjectId(req.params.courseId), //Ищем курс
                    'members.user': {$nin: [ObjectId(req.userId)]}, //с учетом того, что платящий участник в нем не находится
                    'memberCount': {$lt: appConfig.courseMembersMax} // и что в нем нет наполнения
                }, {
                    '$inc': {'memberCount': 1}, //Сразу увеличиваем счетчик участиков
                    '$addToSet': {
                        'members': {
                            'user': ObjectId(req.userId) //И самого участника добавим
                        }
                    }
                }).populate({  //Нужно инфо о программе по которой идет курс
                    'path': 'program',
                    'select': {
                        'fullCost.currency': 1,
                        'fullCost.amount': 1,
                        'title': 1
                    }
                }).execQ();

            var qry = queryPayment.payments(req.userId).getForCourse(req.params.courseId),
                paymentResult = targetCourse
                    .then(function (res) {
                        if (!res) { //Не добавили участника на курс, а может быть он уже там?
                            return Course.find({
                                    '_id': ObjectId(req.params.courseId), //Ищем курс
                                    'members.user': ObjectId(req.userId) //И наш пользователь уже там
                                }, {
                                    '_id': 1,//Нужен только id
                                    'program': 1 //И программа для публикации
                                }
                            ).limit(1)
                                .populate({
                                    'path': 'program',
                                    'select': { //Нужно инфо о программе по которой идет курс
                                        'fullCost.currency': 1,
                                        'fullCost.amount': 1,
                                        'title': 1
                                    }
                                }).execQ()
                                .then(function (foundCourse) {
                                    if (foundCourse.length === 0) { //Если и изменен не был, и так не найден, то курса нет или он уже заполнен
                                        throw 'course already filled or not exists';
                                    } else {
                                        currentProgram = foundCourse[0].program.toJSON(); //Заполняем инфо по программе курса
                                        return currentProgram;
                                    }
                                });
                        }
                        else {
                            currentProgram = res.program.toJSON(); //Заполняем инфо по программе курса
                            return currentProgram;
                        }
                    }).then(userPayment); //Если курс забронирован, можно инициировать оплату

            var getPaymentDescription = function () {
                var elements = [
                    'пользователь ',
                    req.userId,
                    ' оплачивает курс ',
                    req.params.courseId,
                    ' = \'',
                    currentProgram.title,
                    '\''
                ];
                return encodeURIComponent(elements.join(''));
            };

            var getSignature = function (merchantId, transctionId, amount, currencyCode, userId, testMode, veryCode) {
                var signature = String(merchantId) + String(transctionId) + (+amount).toFixed(2) +
                    String(currencyCode) + (userId || '') + (+testMode ? '1' : '0') + veryCode;
                return md5(signature);
            };

            return paymentResult
                .then(function (paymentInfo) {  //'paymentId' 'retryId'
                    var resultUrl = [
                        merchantConfig.merchantUrl,
                        'MNT_ID=',
                        merchantConfig.merchantId,
                        '&MNT_TRANSACTION_ID=',
                        paymentInfo.paymentId,
                        '&MNT_CURRENCY_CODE=',
                        currentProgram.fullCost[0].currency, //TODO: Валюту должен выбирать пользователь
                        '&MNT_AMOUNT=',
                        currentProgram.fullCost[0].amount, //TODO: Надо брать сумму из курса c учетом курса
                        '&MNT_TEST_MODE=',
                        merchantConfig.testMode,
                        '&MNT_DESCRIPTION=',
                        getPaymentDescription(),
                        '&MNT_SUBSCRIBER_ID=',
                        req.userId,
                        '&MNT_SIGNATURE=',
                        getSignature(
                            merchantConfig.merchantId,
                            paymentInfo.paymentId,
                            currentProgram.fullCost[0].amount,
                            currentProgram.fullCost[0].currency,
                            req.userId,
                            merchantConfig.testMode,
                            merchantConfig.veryCode
                        ),
                        '&MNT_CUSTOM1=',
                        paymentInfo.retryId,
                        '&moneta.locale=',
                        merchantConfig.locale
                    ];
                    //Выдаем ответ. В данном случае это строка для редиректа
                    res.cookie('tranId', paymentInfo.paymentId, {httpOnly: true, maxAge: 600000, signed: true}); //10 минут на куку
                    return res.json(
                        {
                            'success': true,
                            'data': resultUrl.join('')
                        }
                    );
                }).catch(function (reason) {
                    //Что-то хлопнулось по дороге и надо устранить участника с курса
                    var errorOutput = function (reason2) {
                        var errors = [{message: reason}];
                        if (reason2) {
                            errors.push({message: reason2});
                        }
                        return res.json({success: false, errorMessages: errors});
                    };
                    return Course.findOneAndUpdate({
                        '_id': ObjectId(req.params.courseId), //Ищем курс
                        'members.user': ObjectId(req.userId) // С нашим участником
                    }, {
                        '$inc': {'memberCount': -1}, //Снимаем счетчик участников
                        '$pull': {
                            'members': {
                                'user': ObjectId(req.userId) //И самого участника удалим
                            }
                        }
                    }).execQ()
                        .then(function () {
                            return errorOutput();
                        }) //Вернем исходную ошибку
                        .catch(errorOutput); //Вернем исходную ошибку и ошибку исключения из группы
                });
        },

        success: function (req, res) {
            /*Сюда приходит пользователь, который выполнил все в платежном интерфейсе по положительному сценарию */
            return tranCookieCheck(
                req.signedCookies.tranId,
                function () {
                    return getFullTran(req.signedCookies.tranId);
                })
                .then(function (content) {
                    return res.clearCookie('tranId')
                        .json(content);
                });
        },

        reject: function (req, res) {
            /*Сюда приходит пользователь, у которого оплата отвалилась */
            return tranCookieCheck(
                req.signedCookies.tranId,
                function () {
                    return closeBill(req.signedCookies.tranId, 'rejected');
                }
            ).then(function (content) {
                    return res.clearCookie('tranId')
                        .json(content);
                });
        },
        failure: function (req, res) {
            /*Cюда приходит пользователь, который сам отказался от оплаты*/
            return tranCookieCheck(
                req.signedCookies.tranId,
                function () {
                    return closeBill(req.signedCookies.tranId, 'failed');
                }
            ).then(function (content) {
                    return res.clearCookie('tranId')
                        .json(content);
                });
        },
        inProgress: function (req, res) {
            /*Cюда приходит пользователь, чей платеж в обработке*/
            return tranCookieCheck(
                req.signedCookies.tranId,
                function () {
                    return getFullTran(req.signedCookies.tranId);
                })
                .then(function (content) {
                    return res.clearCookie('tranId')
                        .json(content);
                });
        },
        confirm: function (req, res) {
            /* Сюда приходит платежный шлюз с информацией о реальной судьбе транзакции */
            console.log(req.body);
            res.set({
                'Content-Type': 'text/plain; charset=utf-8'
            });
            return res.send('FAIL');
        },
        check: function (req, res) {
            /* Сюда приходит платежный шлюз для проверки статуса заказа, суммы и прочих параметров */
            console.log(req.body);
            res.set({
                'Content-Type': 'text/plain; charset=utf-8'
            });
            return res.send('FAIL');
        }
    };
};