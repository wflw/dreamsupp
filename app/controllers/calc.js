var Checklist = require('../models/checklist');

module.exports = function (routes) {
    routes.calc = {};

    routes.calc.process = function (req, res) {
        req.checkBody('timeToReach', 'Время на достижение должно быть чисом').isInt();
        req.checkBody('potential', 'Потенциал должен быть числом').isInt();
        req.checkBody('newness', 'Новизна должна быть числом').isInt();
        req.checkBody('hardness', 'Сложность должна быть числом').isInt();
        req.checkBody('resistance', 'Сопротивление среды должно быть числом').isInt();
        req.checkBody('timeToFill', 'Доступное время должно быть числом').isInt();


        var timeToReach = +req.body.timeToReach,
            potential = +req.body.potential,
            newness = +req.body.newness,
            hardness = +req.body.hardness,
            resistance = +req.body.resistance,
            timeToFill = +req.body.timeToFill;

        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }

        newness = newness/10;
        hardness = hardness/10;
        timeToFill = timeToFill/100;
        var targetPotential = potential*newness,
            effortLevel = (targetPotential-resistance)*hardness, //Если отрицательный, слишком велики усилия, не стоит цель того
            potentialMult = (1/effortLevel),
            potentialTime = (potentialMult/timeToFill)*timeToReach;

        if (effortLevel<=0){
            return res.json({
                success: true,
                data: {
                    validate: ['potential', 'newness', 'resistance']
                }
            });
        }
        return res.json({
            success: true,
            data: {
                result: Number(potentialTime).toFixed(0)
            }
        });
    };
};