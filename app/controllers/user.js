var moment = require('moment'),
    q = require('q'),
    _ = require('underscore'),
    events = require('events').EventEmitter(),

    ObjectId = require('mongoose/lib/').Types.ObjectId,

    User = require('../models/user'),
    Payment = require('../models/payment'),
    Program = require('../models/program'),
    Checklist = require('../models/checklist.js'),
    Course = require('../models/course');

module.exports = function (routes, events) {
    routes.user = {};

    routes.user = {
        getAll: function (req, res) {
            var lastName = req.param('lastName'),
                courseId = req.param('courseId'),
                active   = req.param('active'),
                predicate = {};
            if (lastName){
                predicate['profile.lastName'] = new RegExp(lastName+'.*', 'i');
            }
            if (courseId){
                predicate['courses.course'] = courseId;
            }
            if (active){
                predicate.courses =
                {'$elemMatch': {
                    'dateJoined': {'$exists': true},
                    'dateExited': {'$exists': false}
                }};
            }
            User.find(predicate,{
                'profile':1,
                'role':1,
                'socialProfiles':1,
                'courses.course':1,
                'courses.dateJoined':1,
                'courses.dateExited':1,
                'courses.days': {$slice: -1 }
            }).sort({
                'courses.dateJoined':-1,
                'courses.dateExited':-1
            })
                .populate({
                    path: 'courses.course',
                    select: {
                        'dateStart':1,
                        'dateEnd':1,
                        'program':1
                    }
                })
                .execQ()
                .then(function (usrs) {
                    return Program.populateQ(usrs, {
                        path: 'courses.course.program',
                        select: {
                            'title':1
                        }
                    })
                        .then(function (users){
                            return res.json({success: true, data: users});
                        });
                })
                .catch(function (err) {
                    console.log(err);
                })
                .done();
        },
        acceptToCourse: function(req, res){
            var userId   = ObjectId(req.params.userId),
                courseId = ObjectId(req.params.courseId),

            //Добавляем инфо об оплате всего курса, если её еще нет
                userQ = Payment.findOneQ({
                        'user': userId, //Счет на этого пользователя
                        'course': courseId, //За этот курс
                        'days': {$size: 0}, //Оплата за весь курс
                        'closed': {$exists: true} //И он закрыт
                    }
                ).then(function(payment){
                        if (!payment) {//Если платеж не найден, его нужно создать
                            payment = new Payment({
                                'providerName': 'manual',
                                'user': userId,
                                'course': courseId,
                                'closed': moment().format()
                            }).saveQ().spread(function(pay){
                                    return pay;
                                });
                        }
                        return payment;
                    }).then(function(payment){
                        //Добавляем dateJoined в курсе и в пользователе по курсу
                        return User.findOneAndUpdateQ({
                            '_id': userId, //Наш пользователь
                            'courses': {
                                $elemMatch:{ //Ищем нужный нам курс
                                    'course': courseId, //По Id
                                    'dateJoined': {$exists : false}, //И он еще в него не вступил
                                    'dateExited': {$exists : false}, //И еще не вышел
                                    'purchaseHistory.payment': {$ne: payment._id} //И наш платеж еще не привязан
                                }}
                        },{
                            $set: {
                                'courses.$.dateJoined': moment().format() //Вступаем в группу
                            },
                            $addToSet: {
                                'courses.$.purchaseHistory':{
                                    'payment': payment._id,
                                    'closed' : payment.closed
                                } //Добавляем нашу платежку
                            }
                        });
                    }),

            //Добавляем аналогичную информацию в курсы
                courseQ = Course.updateQ({
                    '_id': courseId,
                    'members.user': userId
                },{
                    $set: {
                        'members.$.dateJoined': moment().format()
                    }
                });

            return q.all([userQ, courseQ])
                .spread(function(user){
                    var result;
                    if (user){
                        var targetCourse = _.find(user.courses, function (course) {
                            return course.course.equals(courseId);
                        });
                        if (targetCourse){
                            result = targetCourse.toJSON();
                            delete result._id;
                            delete result.days;
                            delete result.purchaseHistory;
                            result = {
                                success: true,
                                data: result
                            };
                            events.emit('userAcceptedToCourse', userId, courseId); //Сообщаем, что пользователь принят
                        }
                        else {
                            result = {
                                success: false,
                                errorMessages: ['course not found for user']
                            };
                        }
                    } else {
                        result = {
                            success: false,
                            errorMessages: ['user not found or already joined']
                        };
                    }
                    return res.json(result);
                })
                .catch(function(reason){
                    console.log(reason);
                    return res.json({
                        success: false,
                        errorMessages: [reason]
                    });
                });
        },
        rejectFromCourse: function(req, res){
            var userId   = ObjectId(req.params.userId),
                courseId = ObjectId(req.params.courseId),
            //Добавляем dateExited без dateJoined
            //Удаляем из members соответствующего курса
                userQ = User.findOneAndUpdateQ({
                    '_id': userId,
                    'courses': {
                        $elemMatch:{ //Ищем нужный нам курс
                            'course': courseId, //По Id
                            'dateExited': {$exists : false} //И еще не вышел
                        }}
                },{
                    $set: {
                        'courses.$.dateExited': moment().format() //Вступаем в группу
                    }
                }),
                courseQ = Course.updateQ({
                    '_id': courseId,
                    'members.user': userId
                }, {
                    '$inc': {'memberCount': -1}, //Снимаем счетчик участников
                    '$pull': {
                        'members': {
                            'user': userId //И самого участника удалим
                        }
                    }
                });
            q.all([userQ, courseQ])
                .spread(function(user){
                    var result;
                    if (user){
                        var targetCourse = _.find(user.courses, function (course) {
                            return course.course.equals(courseId);
                        });
                        if (targetCourse){
                            result = targetCourse.toJSON();
                            delete result._id;
                            delete result.days;
                            delete result.purchaseHistory;
                            result = {
                                success: true,
                                data: result
                            };
                            events.emit('userRejectedFromCourse', userId, courseId);
                        }
                        else {
                            result = {
                                success: false,
                                errorMessages: ['course not found for user']
                            };
                        }
                    }
                    else {
                        result = {
                            success: false,
                            errorMessages: ['user not found or not on course']
                        };
                    }
                    return res.json(result);
                }).catch(function(reason){
                    console.log(reason);
                    return res.json({
                        success: false,
                        errorMessages: [reason]
                    });
                });
        }
    };

    routes.user.mergeProfiles = function(req, res){
        req.checkBody('mainUserId', 'Некорректный id основного пользователя').isMongoId();
        req.checkBody('childUserId', 'Некорректный id пользователя-донора').isMongoId();
        var mainUserId = req.body.mainUserId,
            childUserId = req.body.childUserId;

        return User.findOneQ({
            '_id': childUserId
        }, {
            'socialProfiles': 1,
            'courses':1
        }).then(function(user){ //Пользователь нашелся
                return User.updateQ({
                    '_id': mainUserId
                },{
                    '$addToSet': {
                        'socialProfiles': {
                            '$each': user.socialProfiles //Добавляем отсутствующую информацию о соцсетях из донорского аккаунта
                        },
                        'courses': {
                            '$each': user.courses //Добавляем отсутствующую информацию курсах донорского аккаунта
                        }
                    }
                }).then(function(){
                    var checklists = [];
                    for (var i=0; i<user.courses.length; i++){
                        for (var j=0; j<user.courses[i].days.length; j++){
                            var cheklist = user.courses[i].days[j].checklist;
                            if (cheklist){
                                checklists.push(cheklist);
                            }
                        }
                    }
                    return user.removeQ().then(function(){
                        return Course.updateQ({
                            'members.user': childUserId
                        }, {
                            '$set': {
                                'members.$.user': ObjectId(mainUserId)
                            }
                        }, {multi: true}).then(function(){
                            if (checklists.length>0){
                                return Checklist.updateQ({
                                    '_id': {$in: checklists}
                                },{
                                    '$set': {
                                        'user': ObjectId(mainUserId)
                                    }
                                }, {multi: true});
                            } else{
                                return null;
                            }
                        });
                    }); //Удляем пользователя которого слили в основного
                });
            }
        ).then(function(){
                return res.json({success: true, data: 'Профили объединены'});
            }).catch(function(reason){
                console.log(reason);
                return res.json({
                    success: false,
                    errorMessages: [reason]
                });
            });

    };
};