var _ = require('underscore'),
    moment = require('moment'),

    ObjectId = require('mongoose/lib/').Types.ObjectId,
    User = require('../models/user'),
    Checklist = require('../models/checklist'),
    Course = require('../models/course'),
    Program = require('../models/program'),

    appConfig = require('../../config/' + app.get('env') + '/app'),
    marathonComponents = require('../components/marathon');

module.exports = function (routes) {
    routes.marathon = {};

    routes.marathon.saveTargets = function (req, res) {
        var newTarget = '';
        _.each(req.body, function (target) {
            if (target) {
                if (target.name) {
                    newTarget = newTarget + target.name + '\r\n';
                }
            }
        });
        return User.updateQ({
            '_id': req.userId,
            'courses.course': req.params.courseId
        }, {
            '$set': {
                'courses.$.target': newTarget,
                'courses.$.targets': req.body
            }
        }).then(function () {
            return res.json({success: true, data: 'Цели поставлены'});
        }).catch(function (reason) {
            console.log(reason);
            return res.json({success: false, errorMessages: [reason]});
        });
    };

    routes.marathon.getLastStarted = function (req, res) {
        User.findOne({
            _id: req.userId
        }).populate('courses.days.checklist courses.course')
            .execQ()
            .then(function (user) {
                var course = _.find(user.courses, function (item) {
                    return item.course._id.equals(ObjectId(req.params.courseId));
                });
                Program.findById(course.program)
                    .execQ()
                    .then(function (program) {
                        course.program = program;
                        user = user.toJSON();
                        delete user.courses;
                        course = course.toJSON();
                        var lastDay = _.last(course.days);
                        if (lastDay) {
                            course.deadLineTime = moment(appConfig.newDayTime, 'HH:mm:ss')
                                .add(-user.profile.timeZone, 'h')
                                .format('X');
                        }
                        user.course = course;

                        return res.json({success: true, data: user});
                    });
            })
            .catch(function (err) {
                console.log(err);
                return res.json({success: false, errorMessages: [err]});
            })
            .done();
    };


    routes.marathon.addUserDay = function (req, res) {
        req.checkBody('courseId', 'Некорректный идентификатор курса').isMongoId();
        req.checkBody('userId', 'Некорректный идентификатор пользователя').isMongoId();

        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }

        marathonComponents.addUserDay(req.body.userId, req.body.courseId).then(function () {
            return res.json({success: true});
        }).catch(function (err) {
            return res.json({success: false, errorMessages: [err.message]});
        });
    };


    routes.marathon.removeUserDay = function (req, res) {
        req.checkBody('courseId', 'Некорректный идентификатор курса').isMongoId();
        req.checkBody('userId', 'Некорректный идентификатор пользователя').isMongoId();

        var errors = req.validationErrors();
        if (errors) {
            return res.json({
                success: false,
                errorMessages: errors
            });
        }

        marathonComponents.removeUserDay(req.body.userId, req.body.courseId).then(function () {
            return res.json({success: true});
        }).catch(function (err) {
            return res.json({success: false, errorMessages: [err.message]});
        });
    };

};