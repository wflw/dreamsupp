var User = require('../models/user'),
    query = require('../queries/featured');

module.exports = function (routes) {
    routes.featured = {
        get: function (req, res) {
            var qry = query.featuredList(+req.params.perPage).list;
            User.find(qry.filter)
                .select(qry.display)
                .sort({
                    'pos': -1
                })
                .populate({
                    'path': 'courses.course',
                    'select': qry.courseDisplay
                })
                .execQ()
                .then(function (usersWithCourses) {
                    return res.json({success: true, data: usersWithCourses});
                })
                .catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errors: [{message:'database error'}]});
                })
                .done();
        }
    };
};