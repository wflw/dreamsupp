var moment = require('moment'),
    ObjectId = require('mongoose/lib/').Types.ObjectId,
    _ = require('underscore'),

    User = require('../models/user'),
    Course = require('../models/course'),
    Program = require('../models/program'),

    marathonComponents = require('../components/marathon'),

    query = require('../queries/courses');


module.exports = function (routes) {

    var getCoursesQ = function (isStarted) {
        var predicate = {};
        if (isStarted !== undefined) {
            predicate.isStarted = isStarted;
        }
        return Course.find(predicate, {
            'program': 1,
            'created': 1,
            'dateStart': 1,
            'dateEnd': 1,
            'memberCount': 1,
            'socialGroups.groupUrl': 1,
            'socialGroups': {
                '$elemMatch': {
                    'networkName': 'vk'
                }
            }
        }).populate({
                path: 'program',
                select: {
                    'title': 1,
                    'imageUrl': 1
                }
            }
        ).sort({
                'dateStart': -1
            })
            .execQ();
    };

    routes.courses = {
        getNotStarted: function (req, res) {
            return getCoursesQ(false)
                .then(function (courses) {
                    return res.json({success: true, data: courses});
                }).catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errorMessages: [err]});
                });
        },
        getAll: function (req, res) {
            return getCoursesQ()
                .then(function (courses) {
                    return res.json({success: true, data: courses});
                }).catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errorMessages: [err]});
                });
        },
        startCourse: function (req, res) {
            return Course.findOneAndUpdateQ({
                '_id': ObjectId(req.params.courseId)
            }, {
                '$set': {
                    'isStarted': true,
                    'dateStart': moment().format()
                }
            }, {
                '_id': 1,
                'members.user': 1
            }).then(function (course) {
                if (!course) {
                    throw 'Попытка запустить несуществующий или уже запущенный курс';
                }
                _.each(course.members, function (member) {
                        marathonComponents.addUserDay(member.user, course._id);
                    }
                );
                return res.json({success: true, data: 'Курс запущен'});
            })
                .catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errorMessages: [err]});
                })
                .done();
        },
        getDays: function (req, res) {
            /*
             Сначала надо проверить, а есть ли у текущего пользователя право просматривать дни запрошенного пользователя
             Это должен быть или featured курс или одногруппник по курсу (у данного пользователя есть dateJoined по этому id курса)
             */

            var secQuery = {
                '_id': ObjectId(req.params.userId),
                'courses': {
                    '$elemMatch': {
                        'course': ObjectId(req.params.courseId),
                        'isFeatured': true
                    }
                }
            };

            if (req.userId) { //Если мы листаем не аннонимно, то надо еще однокурсников посмотреть
                secQuery = {
                    '$or': [
                        secQuery, {
                            '_id': ObjectId(req.userId),
                            'courses': {
                                '$elemMatch': {
                                    'course': ObjectId(req.params.courseId),
                                    'dateJoined': {'$exists': true}
                                }
                            }
                        }]
                };
            }

            User.find(secQuery).count().execQ().then(function (count) {
                if (count === 0) {
                    throw 'Нет привелегий для просмотра дней заданного пользователя на этом марафоне';
                }
                var qry = query.list(+req.params.perPage).days(req.params.userId, req.params.courseId, +req.params.page);
                return User.find(qry.filter)
                    .select(qry.display)
                    .execQ()
                    .then(function (users) {
                        _.forEach(users, function (user) {
                            user.courses[0].days = _.reject(user.courses[0].days, function (day) {
                                return !day.isAccepted;
                            });
                        });

                        return res.json({success: true, data: users});
                    });
            })
                .catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errorMessages: [err]});
                });
        },
        getNearest: function (req, res) {
            var qry = query.nearest(req.params.programId);
            Course.findOne(qry.filter)
                .select(qry.display)
                .sort(qry.sort)
                .populate({
                    'path': 'members.user',
                    'select': qry.userDisplay
                })
                .populate({
                    'path': 'program',
                    'select': qry.programDisplay
                })
                .execQ()
                .then(function (course) {
                    var result = null;
                    if (course) {
                        var ds = moment(course.dateStart),
                            now = moment(),
                            diff = moment(ds.diff(now));
                        result = course.toJSON();
                        result.timeToEvent =
                        {
                            days: ds.diff(now, 'days'),
                            hours: diff.hours(),
                            minutes: diff.minutes(),
                            seconds: diff.seconds()
                        };
                    }

                    return res.json({success: true, data: result});
                })
                .catch(function (err) {
                    console.log(err);
                    return res.json({success: false, errorMessages: [err]});
                })
                .done();
        }
    };
};
