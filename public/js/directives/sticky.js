duApp.directive('sticky', function () {
    return {
        transclude: true,
        link: function ($scope, element, attrs) {
            element.addClass("fixedsticky");
            attrs.$observe('stickyWhenSet', function(){
                element.fixedsticky();
            }, true);
        },
        template: '<div ng-transclude></div>',
        restrict: 'A'
    };
});


 