duApp.directive('isLoggedIn', ['$rootScope','CurrentUserService',
    function ($rootScope, CurrentUserService) {
    return {
        link: function (scope, element, attrs) {
            var updateVisibility = function () {
                if (CurrentUserService.get().token) {
                    element.removeClass('hidden');
                } else {
                    element.addClass('hidden');
                }
            };

            updateVisibility();
            $rootScope.$on('user::changed', updateVisibility);
        }
    };
}]);
