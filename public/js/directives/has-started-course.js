duApp.directive('hasStartedCourse', [
    '$rootScope',
    'UserCourseService',
    function ($rootScope, UserCourseService) {
        return {
            link: function (scope, element, attrs) {
                var negative = attrs.hasStartedCourse.trim() === 'not';

                var updateVisibility = function () {
                    UserCourseService.hasActiveStartedQ().then(function(isStarted){
                        var hasPermission = negative ? !isStarted : isStarted;
                        if (hasPermission) {
                            element.removeClass('hidden');
                        } else {
                            element.addClass('hidden');
                        }
                    });
                };
                updateVisibility();
                $rootScope.$on('user::activeCourseSet', updateVisibility);
                $rootScope.$on('user::activeCourseChanged', updateVisibility);
            }
        };
    }
]);


 