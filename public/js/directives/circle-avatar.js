duApp.directive('circleAvatar', function () {
    return {
        scope: {},
        link: function ($scope, element, attrs) {
            attrs.$observe('imageUrl', function (imageUrl) {
                $scope.style = {
                    width: attrs.size + 'px',
                    height: attrs.size + 'px',
                    background: 'url(' + imageUrl + ') no-repeat'
                };

                if (attrs.active) {
                    $scope.style.border = '3px solid #e02878';
                }
            });
        },
        template: '<div class="img-circle circle-avatar" ng-style="style"/>',
        restrict: 'E'
    };
});


 