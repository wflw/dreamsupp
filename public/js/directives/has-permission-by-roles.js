duApp.directive('hasPermissionByRoles', [
    '$rootScope',
    'CurrentUserService',
    function ($rootScope, CurrentUserService) {
        return {
            link: function (scope, element, attrs) {
                if (!_.isString(attrs.hasPermissionByRoles)) {
                    throw "hasPermissionByRole role must be a string";
                }

                var roles = attrs.hasPermissionByRoles.trim();

                function updateVisibility() {
                    var hasPermission = CurrentUserService.hasPermissionByRoles(roles);
                    if (hasPermission) {
                        element.removeClass('hidden');
                    } else {
                        element.addClass('hidden');
                    }
                }

                updateVisibility();
                $rootScope.$on('user::changed', updateVisibility);
            }
        };
    }]);


 