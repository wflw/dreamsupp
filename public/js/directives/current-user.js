duApp.directive('currentUser', ['$rootScope', 'CurrentUserService', function ($rootScope, CurrentUserService) {
    return {
        link: function (scope, element, attrs) {
            var updateWidget = function () {
                scope.currentUser = CurrentUserService.get();
            };

            updateWidget();
            $rootScope.$on('user::changed', updateWidget);
        },
        template: '<div style="padding-top:3px;"><div style="display: table-cell; vertical-align: middle; padding-right: 20px;"><a ng-if="!currentUser.firstName" href="/api/auth/vk/" class="btn btn-block btn-social btn-lg btn-vk participate-button" style="width:130px;"><i class="fa fa-vk"></i>Войти</a>{{currentUser.firstName}} {{currentUser.lastName}}</div>' +
        '<div style="display: table-cell"><circle-avatar size="50" image-url="{{currentUser.avatarUrl}}" /></div></div>',
        restrict: 'E'

    };
}]);


 