duAppControllers
    .controller('PaymentResultController', [
        '$scope',
        '$route',
        'BillsService',
        function ($scope, $route, BillsService) {
            $scope.paymentResult = $route.current.paymentResult;
            BillsService.reportBillStatus($scope.paymentResult)
                .then(function (data) {
                    $scope.report = data;
                });
        }]);