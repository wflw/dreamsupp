duAppControllers.controller('ReportController', [
    '$scope',
    'ReportService',
    'CurrentUserService',
    function ($scope, ReportService, CurrentUserService) {
        ReportService.getReports().then(function (reports) {
            $scope.reports = reports;
        });

        $scope.postComment = function (courseId, userId, dayNumber, type, day) {
            var comment = {
                courseId: courseId,
                dayNumber: dayNumber,
                userId: userId,
                verb: type,
                message: day.moderatorComment
            };
            ReportService.postComment(comment).then(function () {
                day.comments.push(
                    {
                        moderator: { profile: CurrentUserService.get()},
                        text: day.moderatorComment,
                        date: Date.now(),
                        verb: type
                    }
                );
                day.moderatorComment = '';
                switch (type) {
                    case 'r':
                        day.rejected = true;
                        break;
                    case 'a':
                        day.accepted = true;
                        break;
                }
            });
        };
    }]);