duAppControllers
    .controller('ProfileController', ['$scope', '$location', '$routeParams', 'CurrentUserService', function ($scope, $location, $routeParams, CurrentUserService) {
        $scope.user = CurrentUserService.get();
    }]);