duAppControllers
    .controller('MyGroupController', [
        '$scope',
        '$rootScope',
        'MyGroupService',
        'CoursesService',
        'UserCourseService',
        function ($scope, $rootScope, MyGroupService, CoursesService, UserCourseService) {

            $scope.daysLoading = true;

            $scope.marginLeft = 0;
            // Avatar width
            $scope.shift = 100;

            function getCurrentGroup() {
                UserCourseService.getActiveIdQ()
                    .then(function (id) {
                        if (id) {
                            MyGroupService.getGroupProgress(id)
                                .then(function (groupProgress) {
                                    $scope.userGroup = groupProgress.course;
                                    $scope.users = groupProgress.users;
                                    $scope.currentFUser = _.first(groupProgress.users);
                                    $scope.userId = $scope.currentFUser._id;
                                    $scope.daysLoading = false;
                                });
                        }
                        else {
                            $rootScope.$broadcast('sec::noPermissions');
                        }
                    });
            }

            getCurrentGroup();

            var groupReact = $rootScope.$on('user::activeCourseChanged', getCurrentGroup);

            $scope.$on('$destroy', function () {
                groupReact();
            });

            $scope.setCurrentFUser = function (user) {
                $scope.userId = user._id;
                $scope.currentFUser = user;
            };

            $scope.getBackgroundByPercent = function (percent) {
                var color = '#a3f1ff';
                if (percent < 71) {
                    color = '#e4ffd9';
                }
                if (percent < 31) {
                    color = '#fffb9e';
                }
                return {background: color};
            };

            $scope.moveLeft = function () {
                if (!$scope.activeArrows()) return;
                var e = document.getElementById('crl');
                $scope.marginLeft += $scope.shift;
                if ($scope.marginLeft > 0) {
                    document.getElementById('left-arrow').style.display = 'table-cell';
                    document.getElementById('left-arrow-stub').style.display = 'none';
                }

                if (($scope.marginLeft + e.offsetWidth) / 100 === $scope.users.length) {
                    document.getElementById('right-arrow').style.display = 'none';
                    document.getElementById('right-arrow-stub').style.display = 'table-cell';
                }
                e.style.marginLeft = '-' + $scope.marginLeft + 'px';
                e.style.marginRight = $scope.marginLeft + 'px';
            };

            $scope.moveRight = function () {
                if (!$scope.activeArrows()) return;
                var e = document.getElementById('crl');
                $scope.marginLeft -= $scope.shift;
                if ($scope.marginLeft < 0) {
                    $scope.marginLeft = 0;
                }
                if ($scope.marginLeft === 0) {
                    document.getElementById('left-arrow').style.display = 'none';
                    document.getElementById('left-arrow-stub').style.display = 'table-cell';
                }
                if (($scope.marginLeft + e.offsetWidth) / 100 < $scope.users.length) {
                    document.getElementById('right-arrow').style.display = 'table-cell';
                    document.getElementById('right-arrow-stub').style.display = 'none';
                }
                e.style.marginLeft = '-' + $scope.marginLeft + 'px';
                e.style.marginRight = $scope.marginLeft + 'px';

            };


            //TODO should be called when resizing
            $scope.activeArrows = function () {
                var e = document.getElementById('crl');
                if ($scope.users) {
                    var show = (($scope.users.length + 2) * $scope.shift) > e.offsetWidth;
                    return show;
                }
                return false;
            };


            $scope.nextDaysPage = function () {
                if (!$scope.daysLoading) {
                    var curCourse = $scope.currentFUser.course;
                    if (!curCourse.currentPage) {
                        curCourse.currentPage = 1;
                    }
                    if (!curCourse.loaded) {
                        $scope.daysLoading = true;
                        curCourse.currentPage++;
                        CoursesService.getDays($scope.userId, $scope.userGroup._id, curCourse.currentPage)
                            .then(function (data) {
                                var newDays = data[0].courses[0].days;
                                if (newDays.length > 0) {
                                    curCourse.days.push.apply(curCourse.days, newDays);
                                } else {
                                    curCourse.loaded = true;
                                }
                                $scope.daysLoading = false;
                            },
                            function () {
                                $scope.daysLoading = false;
                            });
                    }
                }
            };

        }]);