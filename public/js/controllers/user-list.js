duAppControllers
    .controller('UserListController', [
        '$scope',
        '$location',
        '$http',
        'UserService',
        'CoursesService',
        'dialogs',
        function ($scope, $location, $http, UserService,CoursesService, dialogs) {
            $scope.init = function () {
                UserService.getUserList()
                    .then(function (userList) {
                        $scope.userList = userList;
                    }
                );
                CoursesService.getAllCourses()
                    .then(function(courses){
                        $scope.courses =  courses;
                    });
            };

            var filterInit = {
                course: null,
                lastName: null,
                active: null
            };

            $scope.mergeCommand = {}; //Слияние профилей

            $scope.startMerge = function (mainUser){
                $scope.mergeCommand.main = mainUser;
            };

            $scope.finishMerge = function(slaveUser){
                dialogs
                    .confirm('Слияние профилей',
                        'Слияние профилей осуществляется один раз и не может быть отменено в дальнейшем, вы уверены?').
                result.then(function(){
                    $scope.mergeCommand.slave = slaveUser;
                    UserService.mergeProfiles(
                            $scope.mergeCommand.main._id,
                            $scope.mergeCommand.slave._id
                        ).then(function(){
                            $scope.mergeCommand.main.socialProfiles =
                                _.union($scope.mergeCommand.main.socialProfiles,
                                    $scope.mergeCommand.slave.socialProfiles);
                            $scope.mergeCommand.main.courses =
                                _.union($scope.mergeCommand.main.courses,
                                    $scope.mergeCommand.slave.courses);
                            var index = $scope.userList.indexOf($scope.mergeCommand.slave);
                            if (index>=0){
                                $scope.userList.splice(index ,1);

                            $scope.mergeCommand = {};
                        }});
                });
            };

            $scope.clearMerge = function(){
                $scope.mergeCommand = {};
            };

            var updateList = _.debounce(function(filter){
                UserService.getUserList(filter.lastName, filter.course, filter.active)
                    .then(function (userList) {
                        $scope.userList = userList;
                    });
            },500);

            $scope.filters = _.clone(filterInit);

            $scope.$watch('filters', function(newFilter, oldFilter){
                if (_.isEqual(newFilter, oldFilter)) {
                    return;
                }
                updateList(newFilter);
            },true);

            $scope.clearFilters = function(){
                $scope.filters = _.clone(filterInit);
            };

            $scope.init();

            var setDates = function (course, dates) {
                if (course.course._id === dates.course) {
                    if (dates.dateJoined) {
                        course.dateJoined = dates.dateJoined;
                    }
                    if (dates.dateExited) {
                        course.dateExited = dates.dateExited;
                    }
                }
            };

            $scope.accept = function (userId, course) {
                //course._id
                UserService.acceptToCourse(userId, course.course._id)
                    .then(function (newCourseDates) {
                        setDates(course, newCourseDates);
                    });
            };

            $scope.reject = function (userId, course) {
                UserService.rejectFromCourse(userId, course.course._id)
                    .then(function (newCourseDates) {
                        setDates(course, newCourseDates);
                    });
            };

            $scope.addUserDay = function(userId, courseId, days) {
                var params = {
                    userId:userId,
                    courseId: courseId
                };
                $http.put('/api/marathon/addUserDay', params).then(function (response) {
                    if(response.success) {
                        if(!days[0]) {
                            days[0] = {
                                dayNumber : 0
                            };
                        }
                        days[0].dayNumber++;
                        //alert('Пользователю добавлен новый день!');
                    }
                });
            };

            $scope.removeUserDay = function(userId, courseId,day) {
                var params = {
                    userId:userId,
                    courseId: courseId
                };
                $http.put('/api/marathon/removeUserDay', params).then(function (response) {
                    if(response.success) {
                        day.dayNumber--;
                        //alert('Пользователю откатили один день назад!');
                    }
                });
            };

        }]);