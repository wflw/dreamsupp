duAppControllers
    .controller('FeaturedController', [
        '$scope',
        '$window',
        'FeaturedService',
        'BillsService',
        'CoursesService',
        function ($scope, $window, FeaturedService, BillsService, CoursesService) {
            $scope.programId = null;
            $scope.daysLoading = true;

            $scope.$watch('programId', function (programId) {
                if (programId) {
                    CoursesService.getNearestCourse(programId)
                        .then(function (data) {
                            $scope.nearestCourse = data;
                        });
                }
            });

            FeaturedService.getFeatured().then(
                function (data) {
                    $scope.featured = data;
                    $scope.daysLoading = false;
                    if (data.length && data.length > 0) {
                        $scope.currentFUser = data[0];
                        $scope.userId = $scope.currentFUser._id;
                        if ($scope.currentFUser.courses &&
                            $scope.currentFUser.courses.length &&
                            $scope.currentFUser.courses.length > 0) {
                            var firstCourse = $scope.currentFUser.courses[0];
                            $scope.courseId = firstCourse.course._id;
                            $scope.programId = firstCourse.course.program;
                        }
                    }
                },
                function () {
                    $scope.daysLoading = false;
                }
            );

            $scope.setCurrentFUser = function (user) {
                $scope.userId = user._id;
                $scope.currentFUser = user;
            };

            $scope.buy = function (courseId) {
                BillsService.newBill(courseId)
                    .then(function (data) {
                        $window.location.href = data; //Идем на платежный шлюз
                    });
            };

            $scope.getBackgroundByPercent = function (percent) {
                var color = '#a3f1ff';
                if (percent < 71) {
                    color = '#e4ffd9';
                }
                if (percent < 31) {
                    color = '#fffb9e';
                }
                return {background: color};
            };

            $scope.nextDaysPage = function () {
                if (!$scope.daysLoading) {
                    var curCourse = $scope.currentFUser.courses[0];
                    if (!curCourse.currentPage) {
                        curCourse.currentPage = 1;
                    }
                    if (!curCourse.loaded) {
                        $scope.daysLoading = true;
                        curCourse.currentPage++;
                        CoursesService.getDays($scope.userId, $scope.courseId, curCourse.currentPage)
                            .then(function (data) {
                                var newDays = data[0].courses[0].days;
                                if (newDays.length > 0) {
                                    curCourse.days.push.apply(curCourse.days, newDays);
                                } else {
                                    curCourse.loaded = true;
                                }
                                $scope.daysLoading = false;
                            },
                            function () {
                                $scope.daysLoading = false;
                            });
                    }
                }
            };


            $scope.marginLeft = 0;
            // Avatar width
            $scope.shift = 100;

            $scope.moveLeft = function () {
                var e = document.getElementById('crl');
                $scope.marginLeft += $scope.shift;
                if ($scope.marginLeft > 0) {
                    document.getElementById('left-arrow').style.display = 'table-cell';
                    document.getElementById('left-arrow-stub').style.display = 'none';
                }

                if (($scope.marginLeft + e.offsetWidth) / 100 === $scope.featured.length) {
                    document.getElementById('right-arrow').style.display = 'none';
                    document.getElementById('right-arrow-stub').style.display = 'table-cell';
                }
                e.style.marginLeft = '-' + $scope.marginLeft + 'px';
                e.style.marginRight = $scope.marginLeft + 'px';
            };

            $scope.moveRight = function () {
                var e = document.getElementById('crl');
                $scope.marginLeft -= $scope.shift;
                if ($scope.marginLeft < 0) {
                    $scope.marginLeft = 0;
                }
                if ($scope.marginLeft === 0) {
                    document.getElementById('left-arrow').style.display = 'none';
                    document.getElementById('left-arrow-stub').style.display = 'table-cell';
                }
                if (($scope.marginLeft + e.offsetWidth) / 100 < $scope.featured.length) {
                    document.getElementById('right-arrow').style.display = 'table-cell';
                    document.getElementById('right-arrow-stub').style.display = 'none';
                }
                e.style.marginLeft = '-' + $scope.marginLeft + 'px';
                e.style.marginRight = $scope.marginLeft + 'px';

            };


        }]);
