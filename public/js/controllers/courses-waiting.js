duAppControllers
    .controller('CoursesWaitingController', [
        '$scope',
        'CoursesService',
        function ($scope, CoursesService) {
            CoursesService.getWaitingCourses()
                .then(function(courses){
                    $scope.courses = courses;
                });
            $scope.startCourse = function(course){
                CoursesService.startCourse(course._id)
                    .then(function(){
                        $scope.courses.splice(
                            $scope.courses.indexOf(course),1
                        );
                    });
            };
        }
 ]);