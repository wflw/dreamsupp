duAppControllers
    .controller('TokenController', [
        '$scope',
        '$location',
        'CurrentUserService',
        'SessionStorageService',
        function ($scope, $location, CurrentUserService, SessionStorageService) {
            var params = JSON.parse($location.search().json);

            if (params.token) {
                CurrentUserService.set(params);
                $location.url($location.path());

                var returnUrl = SessionStorageService.get('returnPath');
                if (returnUrl){ //Если это после входа, надо возвращаться по пути
                    SessionStorageService.unset('returnPath');
                    $location.path(returnUrl);
                }else {
                    $location.path('/recruiting');
                }
            }
        }]);