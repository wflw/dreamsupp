duAppControllers
    .controller('MarathonController', [
        '$scope',
        '$http',
        '$rootScope',
        '$window',
        '$timeout',
        'UserCourseService',
        function ($scope, $http, $rootScope, $window, $timeout, UserCourseService) {
            $scope.days = [];
            $scope.courseId = '';

            var calcInital = {
                name: '',
                timeToReach: 28,
                potential: 6,
                newness: 6,
                hardness: 7,
                resistance: 1,
                timeToFill: 60
            };

            $scope.savedStatus = {
                checklists: [],
                report: [],
                targets: 's'
            };

            var checkInProgress = function () {
                var isInProgress = function (item) {
                    return item === 'p';
                };
                return _.any($scope.savedStatus.checklists, isInProgress) ||
                    _.any($scope.savedStatus.report, isInProgress) ||
                    isInProgress($scope.savedStatus.targets);
            };

            //Не даем закрыть страницу, если не все сохранилось
            $window.onbeforeunload = function (event) {
                if (!checkInProgress()) {
                    return;
                }
                var message = 'Не все ваши галочки еще сохранились. Вы уверены, что хотите уйти?';
                if (typeof event === 'undefined') {
                    event = $window.event;
                }
                if (event) {
                    event.returnValue = message;
                }
                return message;
            };

            //Не даем пойти по другому пути если еще не все сохранилось
            $scope.$on('$locationChangeStart', function (event) {
                if (checkInProgress()) {
                    event.preventDefault();
                }
            });

            $scope.sectionOpen = {
                task: true,
                targets: true,
                mandatory: true,
                pinned: true,
                simple: true,
                calc: true
            };

            $scope.calc = _.clone(calcInital);

            $scope.calcError = {};
            $scope.calcInProgress = false;

            var calcTarget = _.debounce(
                function (calc) {
                    $http.post('/api/calc', calc)
                        .then(function (response) {
                            $scope.calcInProgress = false;
                            if (response.data.result) {
                                $scope.calcError = {};
                                $scope.daysToTarget = +response.data.result;
                            } else if (response.data.validate) {
                                $scope.daysToTarget = undefined;
                                $scope.calcError = {};
                                _.each(response.data.validate, function (item) {
                                    $scope.calcError[item] = true;
                                });
                            }
                        }, function () {
                            $scope.calcInProgress = false;
                        });
                }, 500);

            var saveCheckList = _.debounce(
                function (checklist, dayNumber) {
                    return $http.put('/api/checklist/' + checklist._id, checklist).then(function () {
                        $scope.savedStatus.checklists[dayNumber] = 's';//Сохранено
                    }, function () {
                        $scope.savedStatus.checklists[dayNumber] = 'e';//Ошибка отложенного сохраниения
                    });
                }, 500);

            var saveTargers = _.debounce(
                function (courseId, targets) {
                    return $http.post('/api/targets/' + courseId, targets).then(function () {
                        $scope.savedStatus.targets = 's';//Сохранено
                    }, function () {
                        $scope.savedStatus.targets = 'e';//Ошибка отложенного сохраниения
                    });
                }, 500);

            var saveReport = _.debounce(function (dayNumber, report, courseId) {
                var data = {
                    courseId: courseId,
                    dayNumber: dayNumber,
                    report: report
                };
                return $http.put('/api/report/save', data).then(function () {
                    $scope.savedStatus.report[dayNumber] = 's'; //Сохранено
                }, function () {
                    $scope.savedStatus.report[dayNumber] = 'e'; //Ошибка отложенного сохраниения
                });
            }, 500);

            //Автоматическое сохранение галочек
            var autoSaveChecklist = function (newChecklist, oldChecklist, dayNumber) {
                if (_.isEqual(newChecklist, oldChecklist)) {
                    return;
                }
                $scope.savedStatus.checklists[dayNumber] = 'p'; //Сохранение в процессе
                saveCheckList(newChecklist, dayNumber);
            };

            //Автоматическое сохранение отчета
            var autoSaveReport = function (newReport, oldReport, dayNumber, courseId) {
                if (!newReport || newReport === '' || newReport === oldReport) {
                    return;
                }
                $scope.savedStatus.report[dayNumber] = 'p'; //Сохранение в процессе
                saveReport(dayNumber, newReport, courseId);
            };


            //Автоматический пересчет дистанции до цели
            $scope.$watch('calc', function (newValue, oldValue) {
                if (_.isEqual(newValue, oldValue)) {
                    return;
                }
                $scope.daysToTarget = undefined;
                $scope.calcInProgress = true;
                calcTarget(newValue);
            }, true);

            //Автоматическое сохранение данных дней
            $scope.$watch('currentDay', function (newDay, oldDay) {
                if (oldDay && newDay.dayNumber === oldDay.dayNumber) {
                    var oldChecklist = oldDay.checklist,
                        newChecklist = newDay.checklist,
                        oldReport = oldDay.report,
                        newReport = newDay.report;
                    autoSaveChecklist(newChecklist, oldChecklist, newDay.dayNumber);
                    autoSaveReport(newReport, oldReport, newDay.dayNumber, $scope.courseId);
                }
            }, true);

            //Автоматическое сохранение целей
            $scope.$watch('targets', function (newTargets, oldTargets) {
                if (!oldTargets || _.isEqual(newTargets, oldTargets)) {
                    return;
                }
                $scope.savedStatus.targets = 'p'; //Сохранение в процессе
                saveTargers($scope.courseId, newTargets);
            }, true);

            $scope.addTarget = function (name, days) {
                $scope.calc = _.clone(calcInital);
                if (!$scope.targets) {
                    $scope.targets = [];
                }
                $scope.targets.push({
                    name: name,
                    daysToReach: days,
                    checked: false
                });
            };

            $scope.removeTarget = function (target) {
                $scope.targets.splice(
                    $scope.targets.indexOf(target), 1
                );
            };

            $scope.setActiveDay = function (day) {
                $scope.currentDay = day;
            };

            // TODO move to service
            // TODO user may have many marathons
            var getMarathon = function () {
                UserCourseService.getActiveIdQ().then(function(id){
                    if (id) {
                        $http.get('/api/marathon/' + id).then(function (response) {
                            if (response.success) {
                                $scope.targets = response.data.course.targets;
                                $scope.days = response.data.course.days;
                                $scope.courseId = response.data.course.course._id;
                                $scope.deadLine = response.data.course.deadLineTime * 1000;
                                $scope.currentDay = _.last($scope.days);
                                $timeout(function() {
                                    $scope.$broadcast('timer-start');
                                }, 500);
                                setSkippedDaysCount();
                            }
                        });
                    } else {
                        $rootScope.$broadcast('sec::noPermissions');                    }
                });
            };

            getMarathon();

            var setSkippedDaysCount = function(){
                var daysNotGiven = 0;
                if ($scope.currentMarathon && $scope.currentMarathon.daysFromStart && $scope.currentMarathon.currentDay){
                    daysNotGiven = $scope.currentMarathon.daysFromStart-$scope.currentMarathon.currentDay;
                }
                if (daysNotGiven<0){
                    daysNotGiven = 0;
                }

                var days = daysNotGiven;
                if ($scope.days){
                    days = days +_.filter($scope.days, function(day){
                        return !day.isAccepted && !day.needToBeChecked;
                    }).length;
                }
                $scope.skippedDaysCount = days;
            };

            var groupReact = $rootScope.$on('user::activeCourseChanged', getMarathon);

            $scope.$on('$destroy', function () {
                groupReact();
                delete $window.onbeforeunload;
            });

            $scope.addCheck = function (type, check) {
                $scope.currentDay.checklist.list[type].push({
                    text: check,
                    isDone: false
                });
            };

            $scope.removeCheck = function (type, index) {
                $scope.currentDay.checklist.list[type].splice(index, 1);
            };

            $scope.pin = function (index) {
                $scope.currentDay.checklist.list.pinned.push($scope.currentDay.checklist.list.simple[index]);
                $scope.currentDay.checklist.list.simple.splice(index, 1);
            };

            $scope.unpin = function (index) {
                $scope.currentDay.checklist.list.simple.push($scope.currentDay.checklist.list.pinned[index]);
                $scope.currentDay.checklist.list.pinned.splice(index, 1);
            };

            //Отправка
            $scope.markCheckNeeded = function (day) {
                var data = {
                    courseId: $scope.courseId,
                    dayNumber: day.dayNumber
                };
                $http.put('/api/report/markCheckNeeded', data).then(function () {
                    day.needToBeChecked = true;
                    setSkippedDaysCount();
                });
            };

            $scope.cancelCheckNeeded = function (day) {
                var data = {
                    courseId: $scope.courseId,
                    dayNumber: day.dayNumber
                };
                $http.put('/api/report/unCheckNeeded', data).then(function () {
                    day.needToBeChecked = false;
                    setSkippedDaysCount();
                });
            };

            $scope.closeReminder = function () {
                $scope.hideReminder = true;
            };
        }]);