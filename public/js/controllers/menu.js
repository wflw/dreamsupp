duAppControllers
    .controller('MenuController', [
        '$scope',
        function ($scope) {
            // TODO move to directive find better way to do it
            $scope.toggleSidebar = function () {
                if (document.getElementById('sidebar').offsetWidth < 250) {
                    document.getElementById('sidebar').style.width = '250px';
                    document.getElementsByClassName('sidebar-nav')[0].style.marginLeft = '0px';
                } else {
                    document.getElementById('sidebar').style.width = '0';
                    document.getElementsByClassName('sidebar-nav')[0].style.marginLeft = '-250px';
                }
            };

        }]);