duAppControllers
    .controller('RecruitingController', [
        '$scope',
        '$rootScope',
        '$http',
        'UserCourseService',
        function ($scope, $rootScope, $http, UserCourseService) {

            $scope.currentCourse = {};

            var getNearestCourse = function() {
                UserCourseService.getNextQ().then(function(currentCourse){
                    if (currentCourse){
                        $scope.currentCourse = currentCourse;
                        $rootScope.currentCourseId = $scope.currentCourse._id;
                        $rootScope.eventTime = $scope.currentCourse.eventTime;
                    }
                });
            };

            getNearestCourse();

            var groupReact = $rootScope.$on('user::activeCourseChanged', getNearestCourse);

            $scope.$on('$destroy', function() {
                groupReact();
            });

            $scope.register = function(){
                $http.get('/api/group/join/'+$scope.currentCourse._id).then(function(){
                        $scope.currentCourse.user.status = 'waiting';
                    }
                );
            };

            //TODO: move to directive
            $scope.getStatusText = function (status) {
                var statusText = 'Зарегистрироваться';

                if (status === 'active') {
                    statusText = 'Участие оплачено';
                } else if (status === 'waiting') {
                    statusText = 'Ждем оплаты';
                } else if (status === 'free'){
                    statusText = 'Вход осуществлен';
                }

                return statusText;
            };
        }]);
