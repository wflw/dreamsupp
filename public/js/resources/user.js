duApp.factory('UserResource', function ($resource) {
    return $resource(
        '/api/user/list',
        {
            id: '@id'
        }, {
            get: {method: 'GET', isArray: true}
        }
    );
});

