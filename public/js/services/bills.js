duApp.service('BillsService', ['$q','$http', function ($q, $http) {
    return {
        newBill: function(courseId){
            {
                var deferred = $q.defer();
                $http.get('/api/bill/create/'+courseId)
                    .then(function (response) {
                        if (response.success) {
                            deferred.resolve(response.data);
                        }
                    });
                return deferred.promise;
            }
        },
        reportBillStatus: function(status){
            var deferred = $q.defer();
            $http.get('/api/bill/'+status)
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    }
                });
            return deferred.promise;
        }
    };
}]);