duApp.factory('CurrentUserService', [
    '$rootScope',
    'SessionStorageService',
    function ($rootScope, SessionStorageService) {
        var currentUser = SessionStorageService.get('currentUser') || {};

        return {
            set: function (user) {
                SessionStorageService.set('currentUser', (user));
                currentUser = user;
                $rootScope.$broadcast('user::changed');
            },
            get: function () {
                return currentUser;
            },
            isLoggedIn: function () {
                return !!currentUser.token;
            },
            hasPermissionByRoles: function (roles) {
                if (_.isString(roles)) {
                    roles = roles.split(',');
                }
                return _.contains(roles, currentUser.role);
            }
        };
    }]);
