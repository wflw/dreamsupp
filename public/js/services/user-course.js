duApp.factory('UserCourseService', [
    '$rootScope',
    '$http',
    '$q',
    function ($rootScope, $http, $q) {

        var availableToRegisterCourseQ = $q.defer(),
            activeCourseQ = $q.defer(),
            availableToRegisterCourseP,
            activeCourseP,

            service = {},

            availableToRegisterCourse,
            activeCourse;

        var assignVariablesFromPromises = function(){
            availableToRegisterCourseP =
                availableToRegisterCourseQ.promise.then(function(data) {
                    availableToRegisterCourse = data;
                    return availableToRegisterCourse;
                });

            activeCourseP =
                activeCourseQ.promise.then(function(data){
                    activeCourse = data;
                    return activeCourse;
                });
        };

        assignVariablesFromPromises(); //При разрешении промисов в полях сервиса сохранится результат

        var setData = function (courseData) {
            availableToRegisterCourseQ.resolve(courseData);
            if (courseData){
                if (courseData.user){
                    var userCourse =  _.find(courseData.user.courses, function(course){
                        return course.dateJoined && !course.dateExited && course.isStarted; //TODO: Вообще пользователь сам должен быбирать среди активных курсов
                    });

                    if (activeCourse){
                        if (!_.isEqual(activeCourse, userCourse)){
                            $rootScope.$broadcast('user::activeCourseChanged', {new: userCourse, old: activeCourse} );
                        }
                    } else {
                        $rootScope.$broadcast('user::activeCourseSet', {new: userCourse, old: null} );
                    }
                    activeCourseQ.resolve(userCourse);
                }
            }
        };

        service.getNextQ = function () {
            return availableToRegisterCourseP;
        };

        service.getActiveQ = function(){
            return activeCourseP;
        };

        service.getActiveIdQ = function () {
            return activeCourseP.then(function(courseData){
                    var result = null;
                    if (courseData){
                        result = courseData.course;
                    }
                    return result;
                }
            );
        };

        service.getNext = function () {
            var result = {};
            if (availableToRegisterCourse){
                result = availableToRegisterCourse;
            }
            return result;
        };

        service.getActive = function(){
            var result = {};
            if (activeCourse){
                result = activeCourse;
            }
            return result;
        };

        service.getActiveId = function () {
            var result = null;
            if (activeCourse){
                result = activeCourse.course;
            }
            return result;
        };

        service.hasActiveStartedQ = function () {
            return this.getActiveQ().then(function(course){
                var hasStarted = false;
                if(course) {
                     hasStarted = course.isStarted && course.dateJoined;
                }
                return hasStarted;
            });
        };

        service.update = function () {

            if (availableToRegisterCourse || activeCourse){
                availableToRegisterCourseQ = $q.defer();
                activeCourseQ = $q.defer();
                assignVariablesFromPromises();
            }

            $http.get('/api/group/active')
                .then(function (response) {
                    if (response.success) {
                        setData(response.data);
                    }
                });
        };

        return service;

    }]);