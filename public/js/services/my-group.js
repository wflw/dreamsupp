duApp.service('MyGroupService', ['$q','$http', 'appConfig', function ($q, $http, appConfig) {
    return {
        myGroups: function(){
                var deferred = $q.defer();
                $http.get('/api/groups/my')
                    .then(function (response) {
                        if (response.success) {
                            deferred.resolve(response.data);
                        }
                    });
                return deferred.promise;
        },
        getGroupProgress: function(courseId){
            var deferred = $q.defer();
            $http.get('/api/group/my/'+courseId+'/'+appConfig.pagination.perPage)
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    }
                });
            return deferred.promise;
        }
    };
}]);