duApp.factory('UserService', [
    '$http',
    '$q',
    'UserResource',
    function ($http, $q, UserResource) {
        return {
            getUserList: function (lastName, course, active) {
                var params = {};
                if (lastName){
                    params.lastName = lastName;
                }
                if (course){
                    params.courseId = course._id;
                }
                if (active){
                    params.active = active;
                }
                return UserResource.get(params).$promise;
            },
            mergeProfiles: function(mainUserId, childUserId){
                return $http.post('/api/user/merge',
                    {
                        mainUserId: mainUserId,
                        childUserId:childUserId
                    });
            },
            acceptToCourse: function (userId, courseId) {
                return $http.get('/api/user/' + userId + '/acceptToCourse/' + courseId)
                    .then(function (response) {
                        return response.data;
                    });
            },
            rejectFromCourse: function (userId, courseId) {
                return $http.get('/api/user/' + userId + '/rejectFromCourse/' + courseId)
                    .then(function (response) {
                        return response.data;
                    });
            }
        };
    }]);
