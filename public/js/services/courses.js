duApp.service('CoursesService', ['$q','$http','appConfig', function ($q, $http, appConfig) {
    return {
        getDays: function(userId, courseId, page){
            var deferred = $q.defer();

            $http.get('/api/userProgress/'+userId+'/'+courseId+'/days/'+appConfig.pagination.perPage+'/'+page)
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    } else{
                        deferred.reject();
                    }
                });
            return deferred.promise;
        },
        getNearestCourse: function(programId){
            var deferred = $q.defer();
            $http.get('/api/course/nearest/'+programId)
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject();
                    }
                });
            return deferred.promise;
        },
        getAllCourses: function(){
            var deferred = $q.defer();
            $http.get('/api/course/all')
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject();
                    }
                });
            return deferred.promise;
        },
        getWaitingCourses: function(){
            return $http.get('/api/course/waiting')
                .then(function(responce){
                    if (!responce.success){
                        throw 'Ошибка получения не стартовавших марафонов';
                    }
                    return responce.data;
                }
            );
        },
        startCourse: function(courseId){
            return $http.post('/api/course/start/'+courseId)
                .then(function(responce){
                    if (!responce.success){
                        throw 'Ошибка старта марафона '+courseId;
                    }
                    return responce.data;
                }
            );
        }
    };
}]);