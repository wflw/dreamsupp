duApp.service('ReportService', [
    '$q',
    '$http',
    function ($q, $http) {
        return {
            getReports: function () {
                var deferred = $q.defer();
                $http.get('/api/report/').then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject();
                    }
                });

                return deferred.promise;
            },

            postComment: function (comment) {
                var deferred = $q.defer();
                $http.post('/api/report/respond', comment).then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject();
                    }
                });
                return deferred.promise;
            }
        };
    }]);
