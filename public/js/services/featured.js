duApp.service('FeaturedService', ['$q','$http','appConfig', function ($q, $http, appConfig) {
    return {
        getFeatured: function () {
            var deferred = $q.defer();

            $http.get('/api/featured/'+appConfig.pagination.perPage)
                .then(function (response) {
                    if (response.success) {
                        deferred.resolve(response.data);
                    }
            });
            return deferred.promise;
        }
    };
}]);
