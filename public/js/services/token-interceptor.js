duApp.factory('TokenInterceptor', [
    '$q',
    '$rootScope',
    'CurrentUserService',
    function ($q, $rootScope, CurrentUserService) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if (CurrentUserService.get().token) {
                    config.headers['x-access-token'] = CurrentUserService.get().token;
                }
                return config;
            },

            requestError: function (rejection) {
                return $q.reject(rejection);
            },

            response: function (response) {
                // Если с сервера пришел не шаблон, проверим на ошибки
                if (!_.isString(response.data)) {
                    response = response.data;
                    if (!response.success) {
                        var errorMessage = '';
                        for (var i = 0; i < response.errorMessages.length; i++) {
                            errorMessage += response.errorMessages[i] + '\r\n';
                        }
                        $rootScope.$broadcast('global::responseFailure', errorMessage);
                    }
                }
                return response || $q.when(response);
            },

            /* Revoke client authentication if 401 is received */
            responseError: function (rejection) {
                if (rejection !== null){
                    var errorMessage;
                    if (rejection.data){
                        if (rejection.data.errorMessages){
                            errorMessage = '';
                            for (var i = 0; i < rejection.data.errorMessages.length; i++) {
                                errorMessage += rejection.data.errorMessages[i] + '\r\n';
                            }
                        }
                    }

                    if (rejection.status === 401){
                        if (CurrentUserService.get().token) {
                            $rootScope.$broadcast('sec::sessionFailure', {clientMessage: 'Ваша сессия истекла', serverMessage: errorMessage});
                        } else {
                            $rootScope.$broadcast('sec::sessionFailure', {clientMessage: 'Вы не не осуществили вход' ,serverMessage: errorMessage});
                        }
                    }
                    if (rejection.status === 403){
                        $rootScope.$broadcast('sec::noPermissions');
                    }
                }
                return $q.reject(rejection);
            }
        };
    }]);