var duApp = angular.module("duApp", [
    'ngSanitize',
    'ngRoute',
    'ngResource',
    'duApp.controllers',
    'ui.bootstrap',
    'nsPopover',
    'infinite-scroll',
    'timer',
    'ui.select',
    'dialogs.main',
    'pascalprecht.translate'
]);

var duAppControllers = angular.module('duApp.controllers', []);

duApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
});

duApp.config(function ($routeProvider) {
    $routeProvider.when('/profile', {
        templateUrl: '/partials/profile.html',
        controller: 'ProfileController',
        roles: 'coach,student'
    });

    $routeProvider.when('/report', {
        templateUrl: '/partials/report.html',
        controller: 'ReportController',
        roles: 'coach'
    });

    $routeProvider.when('/user-list', {
        templateUrl: '/partials/user-list.html',
        controller: 'UserListController',
        roles: 'coach'
    });

    $routeProvider.when('/marathon', {
        templateUrl: '/partials/marathon.html',
        controller: 'MarathonController',
        roles: 'coach,student'
    });

    $routeProvider.when('/featured', {
        templateUrl: '/partials/featured.html',
        controller: 'FeaturedController',
        roles: ''
    });

    $routeProvider.when('/recruiting', {
        templateUrl: '/partials/recruiting.html',
        controller: 'RecruitingController',
        roles: ''
    });

    $routeProvider.when('/ambar', {
        templateUrl: '/partials/ambar.html',
        controller: 'RecruitingController',
        roles: ''
    });

    $routeProvider.when('/my-group', {
        templateUrl: '/partials/my-group.html',
        controller: 'MyGroupController',
        roles: ''
    });

    $routeProvider.when('/courses-waiting', {
        templateUrl: '/partials/courses-waiting.html',
        controller: 'CoursesWaitingController',
        roles: 'coach'
    });


    $routeProvider.when('/token', {
        template: '',
        controller: 'TokenController',
        roles: ''
    });

    $routeProvider.when('/success', {
        templateUrl: '/partials/payment-result.html',
        controller: 'PaymentResultController',
        paymentResult: 'success'
    });

    $routeProvider.when('/failure', {
        templateUrl: '/partials/payment-result.html',
        controller: 'PaymentResultController',
        paymentResult: 'failure'
    });

    $routeProvider.when('/reject', {
        templateUrl: '/partials/payment-result.html',
        controller: 'PaymentResultController',
        paymentResult: 'reject'
    });

    $routeProvider.when('/inProgress', {
        templateUrl: '/partials/payment-result.html',
        controller: 'PaymentResultController',
        paymentResult: 'inProgress'
    });

    $routeProvider.when('/about', {
        templateUrl: '/partials/about.html',
        controller: ''
    });

    $routeProvider.when('/conditions', {
        templateUrl: '/partials/conditions.html',
        controller: ''
    });

    $routeProvider.when('/cost', {
        templateUrl: '/partials/cost.html',
        controller: ''
    });

    $routeProvider.when('/contacts', {
        templateUrl: '/partials/contacts.html',
        controller: ''
    });

    $routeProvider.when('/wrongGender', {
        templateUrl: '/partials/wrong-gender.html',
        controller: ''
    });

    $routeProvider.when('/no-permission', {
        templateUrl: '/partials/no-permission.html'
    });

    $routeProvider.otherwise({redirectTo: '/recruiting'});

});

duApp.config(['$translateProvider',function($translateProvider){
    $translateProvider.translations('ru-RU',{
        DIALOGS_ERROR: "Ошибка",
        DIALOGS_ERROR_MSG: "Обнаружена ошибка.",
        DIALOGS_CLOSE: "Закрыть",
        DIALOGS_PLEASE_WAIT: "Пожалуйстса, подождите",
        DIALOGS_PLEASE_WAIT_ELIPS: "Пожалуйстса, подождите...",
        DIALOGS_PLEASE_WAIT_MSG: "Ожидаем, пока операция завершится.",
        DIALOGS_PERCENT_COMPLETE: "% Завершено",
        DIALOGS_NOTIFICATION: "Оповещение",
        DIALOGS_NOTIFICATION_MSG: "Неизвестное оповещение.",
        DIALOGS_CONFIRMATION: "Потдтверждение",
        DIALOGS_CONFIRMATION_MSG: "Требуется подтверждение операции.",
        DIALOGS_OK: "OK",
        DIALOGS_YES: "Да",
        DIALOGS_NO: "Нет"
    });

    $translateProvider.preferredLanguage('ru-RU');
}]);

duApp.run([
    '$rootScope',
    '$location',
    'dialogs',
    'CurrentUserService',
    'UserCourseService',
    'SessionStorageService',
    function ($rootScope, $location, dialogs, CurrentUserService, UserCourseService, SessionStorageService) {

        var needLoginDialog = function(message){
            if (!$rootScope.loginRequred){
                SessionStorageService.set('returnPath', $location.path());
                $rootScope.loginRequred = true;
                dialogs.create('/partials/login.html', 'LoginController', message).result
                    .then(
                    undefined, //В данном случае диалог никогда не будет разрешен, т.к. мы уйдем в VK
                    function(){
                        delete $rootScope.loginRequred;
                        SessionStorageService.unset('returnPath');
                        $location.path('/no-permission'); //В случае отмены, что делать, нет прав
                    }
                );
            }
        };

        $rootScope.$on('global::responseFailure', function(event, message){
            dialogs.error('Ошибка запроса к серверу', message);
        });

        $rootScope.$on('sec::sessionFailure', function(event, message){ //Если сессия недоступна, сообщаем
            needLoginDialog(message);
        });

        $rootScope.$on('sec::noPermissions', function(){ //Если пришло "нетправ" с сервера
            $location.path('/no-permission');
        });

        $rootScope.$on('user::activeCourseSet', function(event, newCourse){
            $rootScope.currentMarathon = newCourse.new;
        });

        $rootScope.$on('user::activeCourseChanged', function(event, newCourse){
            $rootScope.currentMarathon = newCourse.new;
        });

        $rootScope.$on('$routeChangeStart', function (event, next, current) {

            //TODO: shouldnt be called in each route, but may be useful
            if (next.$$route &&
                next.$$route.controller &&
                next.$$route.controller!=='TokenController'){ //TODO: Костыль от ненужного предварительного запроса на курсы в случае выставления токена
                UserCourseService.update();
            }

            var roles = (next.$$route) ? next.$$route.roles : '';
            if (!_.isEmpty(roles) && !CurrentUserService.hasPermissionByRoles(roles)) {
                event.preventDefault();
                if (CurrentUserService.isLoggedIn()){ //Если пользователь есть, то нет прав
                    $location.path('/no-permission');
                } else{ //В противном случае нужно осуществить вход
                    needLoginDialog(
                        {clientMessage: 'Страница доступна только для зарегистрированных пользователей' ,serverMessage: 'Вы не авторизированы'});
                }
            } else {
                $rootScope.activeRoute = next.$$route ? next.$$route.originalPath.substr(1) : '';
            }
        });
    }]);
