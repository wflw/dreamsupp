duApp.filter('lineBreak', function () {
    return function (input) {
        return input ? input.replace(/(?:\r\n|\r|\n)/g, '<br />') : '';
    };
});
