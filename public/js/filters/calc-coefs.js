duApp.filter('calcCoefs', function () {
    return function (input, type) {
        var result = input,
            word = '';
        if (!isNaN(input))
        {
            switch (type)
            {
                case 'potential':
                    if (input<3){
                        word = 'Почти никогда не достигала';
                    } else if (input<5){
                        word = 'Редко достигала';
                    } else if (input<7){
                        word = 'Достигала';
                    } else {
                        word = 'Легко достигала';
                    }
                    break;
                case  'newness':
                    if (input<3){
                        word = 'Почти никогда или никогда не сталкивалась с ';
                    } else if (input<5){
                        word = 'Имею общее преставление о достижении ';
                    } else if (input<7){
                        word = 'Понимаю как достичь, и готова составить план по достижению ';
                    } else {
                        word = 'В голове уже есть план по достижению ';
                    }
                    break;
                case  'hardness':
                    if (input<3){
                        word = 'Нужно очень сильно постараться, чтобы получить ';
                    } else if (input<5){
                        word = 'Будет тяжеловато достичь ';
                    } else if (input<7){
                        word = 'Потребуются некоторые усислия, но я достигну ';
                    } else {
                        word = 'Достигну легко и без особых усилий ';
                    }
                    break;
                case  'resistance':
                    if (input<3){
                        word = 'Нет никаких преград для достижения ';
                    } else if (input<5){
                        word = 'Есть трудности для достижения ';
                    } else if (input<7){
                        word = 'Имеются достаточно серъезные трудности для достижения ';
                    } else {
                        word = 'Все и всё на свете против достижения ';
                    }
                    break;
            }
            if (word){
                result = '('+result+') '+word;
            }
        }
        return result;
    };
});