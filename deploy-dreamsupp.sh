#!/bin/bash 

cd /var/www
rm -rf dreamsupp-test
git clone https://github.com/workflowsoft/dreamsupp.git ./dreamsupp-test
cd dreamsupp-test
git checkout $1
npm install
bower install
chmod -R 777 ./
