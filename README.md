dreamsupp
==========

Сервис достижения целей

Технологии, используемые на проекте
----------------------------------
* Платформа [NodeJs](http://nodejs.org/) v0.10.33
* База данных [MongoDB](http://www.mongodb.org/) v2.6
* Менеджер зависимостей для NodeJs [npm](https://www.npmjs.org/)
* Менеджер зависимостей для фронта [Bower](http://bower.io/)
* Таск менеджер для фронта [Grunt](http://gruntjs.com/)
* Фреймворк [Express](http://expressjs.com/)
* Js фреймворк [Angular](http://angularjs.org/)
* Js библиотека [Underscore](http://underscorejs.org/)
* Front-end фреймворк [Bootstrap](http://getbootstrap.com/)
* Веб-сервер [Nginx](http://nginx.org/ru/)


Развертвание проекта на новой машине (Linux)
------------------------------------
* Устанавливаешь необходимые пакеты
```
apt-get install python-software-properties
apt-add-repository ppa:chris-lea/node.js
apt-get update
apt-get install nginx mongodb nodejs npm
```
* Чтобы установить свеженькую mongodb на твоей новомодной Ubuntu в корне проекта `sh ubuntu-mongo2.6.sh`
* Настройка nginx. Создавай `/etx/nginx/sites-enabled/dreamsupp` с содержимым
```
server {
    listen 80;
    server_name dreamsupp.local;
    root /home/shart/Dropbox/Apps/dreamsupp/public/;
    etag on;

    location / {
        index index.html;
    }

    #proxy to nodejs
    location /api/ {
		proxy_pass http://127.0.0.1:3000;
	}

    error_page 404 /index.html;

	gzip            on;
	gzip_min_length 100;
	gzip_proxied    expired no-cache no-store private auth;
	gzip_types      text/plain application/xml application/json;

}
```
* Добавь в `/etc/hosts` строку `127.0.0.1	dreamsupp.local`
* Перезапусти nginx `sudo service nginx restart`
* Чтобы forever сам перезапускался после перезагрузки системы добавь в `/etc/rc.local` строчку `su shart -c 'forever start /var/www/dreamsupp-prod/server.js'`
* Клонируешь репозиторий `git clone https://github.com/workflowsoft/dreamsupp.git`
* Переходишь в папку с проектом и затягиваешь зависимости `npm install && bower install`
* Глобально устанавливаешь phantomjs `sudo npm install phantomjs -g`
* Запусти тест vkphantom чтобы убедится, что он работает и заапрувить айпишник `node tests/vkphantom.js`
* Запускай nodejs сервер из корня проекта и наслаждайся `node server.js`
* Для того чтобы загрузить тестовые данные (seeds) в свою локальную базу набирай в корне проекта 
```
node seed names=programs type=json
node seed names=program-28-day-success type=js
node seed names=program-demo type=json append=true
node seed names=course-demo type=js
node seed names=users,featured-users type=js
``` 


Вылив на тест, прод
----------------------------------
* Бежишь на вритуалку
* Если ты в первый раз на виртуалочке, настраивай дамп mongodb из crontab в корне
* Переходишь в `/var/www`
* Деплоишь тест `sh deploy-dreamsupp.sh`
* Если надо залить из ветки, то `sh deploy-dreamsupp.sh develop`
* Накатываешь сиды, если нужно `node dreamsupp-test/seed names=users type=js`
* Мигрируешься если нужно `node dreamsupp-test/migrate`
* Перезапускаешь сервер `forever restart dreamsupp-test/server.js`
* Если надо задеплоить из теста на прод `sh deploy-dreamsupp-to-prod.sh`
* Катишь сиды и миграции, как на тесте
* Перезапускаешь сервер `forever restart dreamsupp-prod/server.js`
