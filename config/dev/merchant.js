module.exports = {
    merchantUrl: 'https://demo.moneta.ru/assistant.htm?', //Тестовый сервер
    merchantId: 89328426, //Тестовый номер счета
    password: '66955', //Платежный пароль
    testMode: 1, //Тестовый режим, включен
    locale: 'ru', //Локаль интерфейса платежного шлюза
    veryCode: 432254 //Код проверки целостности данных
};