var vkConfigTest = {
    clientID: 4561607, // API_ID of dreamsupp-test.workflowsoft.co
    clientSecret: 'YuCg0PaUlDtzOYtiHpeq',
    callbackURL: 'http://dreamsupp-test.workflowsoft.co/api/auth/vk/callback',
    apiVersion: '5.25',
    permissions: ['friends']
};

var vk = {
    getOauth1 : function(courseId) {
        var config = vkConfigTest;
        var oauth1 = [
            'https://oauth.vk.com/authorize?client_id=',
            config.clientID,
            '&scope=',
            config.permissions.join(),
            '&redirect_uri=',
            config.callbackURL,
            '&response_type=',
            'code',
            '&v=',
            config.apiVersion,
            courseId? '&state=':'',
            courseId? courseId: ''
        ];

        return oauth1.join('');
    },
    getOauth2 : function(code) {
        var config = vkConfigTest;
        var oauth2 = [
            'https://oauth.vk.com/access_token?client_id=',
            config.clientID,
            '&client_secret=',
            config.clientSecret,
            '&code=',
            code,
            '&redirect_uri=',
            encodeURIComponent(config.callbackURL)
        ];

        return oauth2.join('');
    }
};

module.exports.vk = vk;
module.exports.secretToken = 'IreallyHateFuckingGypsy';
