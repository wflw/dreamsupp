db.programs.find().forEach(function (program) {
    program.days.forEach(function (day) {
        if (!day.dayNumber && day.day) {
            if (day.day) {
                day.dayNumber = day.day;
                delete day.day;
            } else {
                print('day.day is empty for program._id = ' + program._id);
            }
        } else {
            print('DayNumber seems to be ok for program._id = ' + program._id + ' and day = ' + day.dayNumber);

        }
    });

    db.programs.save(program);
});


db.users.find().forEach(function (user) {
    user.courses.forEach(function (course) {
        course.days.forEach(function (day) {
            if (!day.dayNumber) {
                if (day.day) {
                    day.dayNumber = day.day;
                    delete day.day;
                } else {
                    print('day.day is empty for user._id = ' + user._id);
                }
            } else {
                print('DayNumber seems to be ok for user._id = ' + user._id + ' and day = ' + day.dayNumber);
            }
        });
    });

    db.users.save(user);
});