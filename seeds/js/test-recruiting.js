// TODO чтобы сработало надо запускать отдельно
// пофиксить асинхронное говно
var User = require('../../app/models/user.js'),
    Course = require('../../app/models/course.js');

var updateCourse = function(user){
    return Course.updateQ({
            '_id':  user.courses[0].course,
            'members.user': user._id
        }, {
            $unset: {'members.$.dateJoined': 1}
        }
    ).then(function(updateRes){
            console.log('course updated');
            console.log(updateRes);
        });
};

module.exports.init = function () {
    //Сделаем, чтобы Jax, Opie и Happy был наминально включен в свою единственную группу.

    User.findQ({
            'profile.firstName' : {
                $in: ['Jax','Opie', 'Happy']
            }
        })
        .then(function(users){
            var updChain;
            users.forEach(function(user){
                user.courses[0].dateJoined = undefined;
                user.courses[0].dateExited = undefined;
                if (!updChain){
                    updChain = updateCourse(user);
                } else {
                    updChain = updChain.then(function() {
                        return updateCourse(user);
                    });
                }
                updChain = updChain.then(function(){
                    return user.saveQ().then(function(user){
                            console.log('user saved '+user._id);
                        }
                    );
                });
            });
            return updChain;
        }).catch(function(reason){
            console.log(reason);
        }).done();
};