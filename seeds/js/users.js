var moment = require('moment');
console.log(moment().format());

var User = require('../../app/models/user.js');
var Course = require('../../app/models/course.js');
var Program = require('../../app/models/program.js');
var Checklist = require('../../app/models/checklist.js');

function insertUser(user, course, key) {
    var checklist = {
        course: course._id,
        dayNumber: 1,
        list: {
            mandatory: [
                {
                    text: 'Улыбнуться',
                    isDone: true
                }
            ],
            pinned: [
                {
                    text: 'Проснуться не позже 7:00',
                    isDone: false
                }
            ],
            simple: [
                {
                    text: 'Купить красную юбку',
                    isDone: false
                }
            ]
        }
    };
    var checklistModel = new Checklist(checklist);
    checklistModel
        .saveQ()
        .then(function (cl) {
            console.log('Checklist saved');
            return cl;
        }).then(function (checklist) {
            user.courses = [];
            user.courses.push({
                course: course._id,
                dateJoined: dateStart,
                dateExited: dateEnd,
                days: []
            });
            user.courses[0].days.push({
                dayNumber: 1,
                checklist: checklist._id,
                report: 'Я все сделала, я молодец!',
                needToBeChecked: true
            });
            var newUserModel = new User(user);
            //Плохо так делать, если ебнется, выполнение не остановится
            newUserModel
                .saveQ()
                .then(function (user) {
                    console.log('User #' + key + ' saved');
                    console.log(user._id);
                    course.members.push({user: user._id, dateJoined: dateStart});
                    course.memberCount++;
                    course
                        .saveQ()
                        .then(function () {
                            //сохраняем обратную связь пользователь в чеклисте
                            checklistModel.user = user._id;
                            checklistModel.saveQ();
                        });
                })
                .done();

        })
        .then()
        .done();
}


var users = [
    {
        "role": "coach",
        "profile": {
            "firstName": "Jax",
            "lastName": "Teller",
            "avatarUrl": "http://img2.wikia.nocookie.net/__cb20140909232225/sonsofanarchy/images/5/57/Jax_607.png",
            "timeZone": 3
        },
        "socialProfiles": [
            {
                "networkName": "vk",
                "profileId": "125112070",
                "profileUrl": "http://vk.com/kate_razor"
            }
        ]
    },
    {
        "role": "coach",
        "profile": {
            "firstName": "Chibs",
            "lastName": "Telford",
            "avatarUrl": "http://img1.wikia.nocookie.net/__cb20140909232339/sonsofanarchy/images/8/8b/Chibs_603.png",
            "timeZone": 4
        },
        "socialProfiles": [
            {
                "networkName": "vk",
                "profileId": "125112070",
                "profileUrl": "http://vk.com/kate_razor"
            }
        ]

    },
    {
        "role": "student",
        "profile": {
            "firstName": "Tig",
            "lastName": "Trager",
            "avatarUrl": "http://img2.wikia.nocookie.net/__cb20140822210404/sonsofanarchy/images/4/4a/Tig_607.jpg",
            "timeZone": -1
        },
        "socialProfiles": [
            {
                "networkName": "vk",
                "profileId": "125112070",
                "profileUrl": "http://vk.com/kate_razor"
            }
        ]
    },
    {
        "role": "student",
        "profile": {
            "firstName": "Opie",
            "lastName": "Winston",
            "avatarUrl": "http://img2.wikia.nocookie.net/__cb20140822221130/sonsofanarchy/images/2/24/Opie_411.jpg",
            "timeZone": 3
        },
        "socialProfiles": [
            {
                "networkName": "vk",
                "profileId": "125112070",
                "profileUrl": "http://vk.com/kate_razor"
            }
        ]
    },
    {
        "role": "student",
        "profile": {
            "firstName": "Happy",
            "lastName": "Lowman",
            "avatarUrl": "http://img3.wikia.nocookie.net/__cb20140822205603/sonsofanarchy/images/9/9e/Happy_603.jpg",
            "timeZone": 12
        },
        "socialProfiles": [
            {
                "networkName": "vk",
                "profileId": "125112070",
                "profileUrl": "http://vk.com/kate_razor"
            }
        ]
    }
];

var dateStart = moment().add(-7, 'd').format(),
    dateEnd =  moment().add(21, 'd').format(),
    course = {
        created: dateStart,
        // через 7 дней
        dateStart: dateStart,
        // 28 дней с момента старта
        dateEnd: dateEnd,
        socialGroups: [
            {
                networkName: 'vk',
                groupId: 'rasorium',
                groupUrl: 'http://vk.com/rasorium'
            }
        ],
        members: [],
        isStarted: true
    };

module.exports.init = function (append, env) {
    if (env === 'prod' || env === 'test'){
        Course.removeQ();
        Checklist.removeQ();
        User.removeQ().then(function(){
            (new User(
                {"profile" : {
                    "firstName" : "Dreams",
                    "lastName" : "Upp",
                    "avatarUrl" : "http://cs624621.vk.me/v624621065/7a44/FQJgLBzqQD4.jpg",
                    "timeZone" : "4"
                },
                "role" : "coach",
                "socialProfiles" : [{
                    "networkName" : "vk",
                    "profileId" : "272105065",
                    "profileUrl" : "http://vk.com/dreamsupp"
                }]
               }
            )).saveQ().then(function(){
                    console.log('dreamsupp user saved');
                });
        });
    }   else {
        Program.findOne({title: 'Успех за 28 дней'})
            .execQ()
            .then(function (program) {
                if (program === null) throw 'empty program!';
                course.program = program._id;
                return course;
            })
            .then(function (course) {
                if (!append) {
                    Course.removeQ();
                }

                var courseModel = new Course(course);
                return courseModel
                    .saveQ()
                    .then(function () {
                        console.log('Test course saved saved');
                        return courseModel;
                    });
            })
            .then(function (course) {
                if (!append) {
                    User.removeQ();
                    Checklist.removeQ();
                }

                for (var key in users) {
                    insertUser(users[key], course, key);
                }

            })
            .catch(function (err) {
                console.log(err);
            });
    }

    return module.exports;
};
