var moment = require('moment'),
    q = require('q'),

    Course = require('../../app/models/course.js'),
    Program = require('../../app/models/program.js'),

    startDate = moment("30.12.2014 00:00", "DD.MM.YYYY HH:mm"),
    endDate = moment(startDate).add(1, 'y');

var course = {
    created: startDate.format(),
    dateStart: startDate.format(),
    dateEnd: endDate.format(),
    socialGroups: [
        {
            networkName: 'vk',
            groupId: 'dreamsupp_demo',
            groupUrl: 'https://vk.com/dreamsupp_demo'
        }
    ],
    members: [],
    isStarted: true
};

module.exports.init = function () {
    Program.findOne({title: 'Успех за 28 дней. Демо'}).execQ()
        .then(function (program) {
            if (program === null) {
                throw 'empty program!';
            }
            course.program = program._id;
            return course;
        })
        .then(function (course) {
            var courseModel = new Course(course);
            return courseModel
                .saveQ()
                .then(function (newCourse) {
                    console.log('Demo course course saved');
                    return newCourse;
                })
                .catch(function (err) {
                    console.log(err);
                });
        })
        .catch(function (err) {
            console.log(err);
        })
        .done();
};