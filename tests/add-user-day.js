/**
 * usage
 * node tests/add-user-day.js userId=546ca51548a5e8bd1def129a courseId=546ca51548a5e8bd1def127b
 */
var mongoose = require('mongoose-q')(require('mongoose'), {spread: true}),

    env = require('../env'),
    dbConfig = require('../config/' + env + '/db'),
    marathonComponents = require('./marathon'),
    User = require('../app/models/user'),
    Course = require('../app/models/course'),
    Checklist = require('../app/models/checklist'),
    Program = require('../app/models/program');

mongoose.connect(dbConfig.url);

var userId,
    courseId;

if (env !== 'dev') {
    console.log('Can run only on dev machine');
    process.exit(1);
}

for (var k in process.argv) {
    var arg = process.argv[k];
    if (arg.search('userId=') === 0) {
        userId = arg.slice(7);
    }
    if (arg.search('courseId=') === 0) {
        courseId = arg.slice(9);
    }
}

if (!userId) {
    console.log('No user id!');
    process.exit(1);
}
if (!courseId) {
    console.log('No course id!');
    process.exit(1);
}


marathonComponents.addUserDay(userId, courseId);
