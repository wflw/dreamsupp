var env = require('../env'),
    express = require('express');

app = express();
app.set('env', env);

var vkPh = require('../app/components/vkphantom');
vkPh.execute(vkPh.locationApproveAction)
    .then(function (u) {
        console.log('Processed ');
        console.log(u);
    })
    .catch(function (errors) {
        console.log('Error: ', errors);
    });