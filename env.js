var dir = __dirname.split('/').pop(),
    env = 'dev';

env = dir === 'dreamsupp-prod' ? 'prod' : env;
env = dir === 'dreamsupp-test' ? 'test' : env;

module.exports = env;