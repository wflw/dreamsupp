var newrelic = require('newrelic'),
    env = require('./env'),
    express = require('express'),
    moment = require('moment'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    mongoose = require('mongoose-q')(require('mongoose'), {spread: true}),
    expressValidator = require('express-validator'),

// CronJob = require('cron').CronJob,
    dbConfig = require('./config/' + env + '/db'),
    serverConfig = require('./config/' + env + '/server'),
    appConfig = require('./config/' + env + '/app');

//HACK: Здесь не var app = .. по той причине, что нужна гобальная видимость app, сделано намерянно
app = express();
app.set('env', env);

//Нам интересны параметры в теле
app.use(bodyParser.urlencoded({extended: false}));

//И JSON тоже в теле интересен
app.use(bodyParser.json());

app.use(expressValidator({
    errorFormatter: function (param, msg) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return 'Параметр: \'' + formParam + '\' указан неверно \'' + msg + '\'';
    }
}));

//Добавим обработчик Cookie c подписью содержимого
app.use(cookieParser(appConfig.cookieSecret));

//Указание параметров-регулярных выражений в путях
app.param(function (name, fn) {
    if (fn instanceof RegExp) {
        return function (req, res, next, val) {
            var captures = fn.exec(String(val));
            if (captures) {
                if (captures.length > 0) {
                    if (captures[0]) {
                        req.params[name] = captures[0];
                    }
                }
                next();
            } else {
                next('route');
            }
        };
    }
});

mongoose.connect(dbConfig.url);
//Все хранится и обрабатывается в UTC
process.env.TZ = 'Europe/Greenwich';

require('./app/routes.js')(app);

// TODO enable cron only prod prod
//var courseReadyCron = require('./crons/course-ready');
//var newDayCron = require('./crons/new-day');
//var notificationCron = require('./crons/notifications');
//var job = new CronJob('* * * * * *', function () {
//    newDayCron();
//    courseReadyCron();
//}, null, true);

//var newDayCron = require('./crons/new-day');
//newDayCron();

process.on('uncaughtException', function (err) {
    console.trace(err);
});

var server = app.listen(serverConfig.port, function () {
    console.log('Listening on port %d', server.address().port);
});
