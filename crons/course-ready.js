var moment = require('moment');

var Course = require('../app/models/course');

module.exports = function () {
    Course
        .find()
        .execQ()
        .then(function (courses) {
            for (var k in courses) {
                var course = courses[k];
                var diff = moment().diff(course.dateStart, 's');
                // Если время пришло, а группы нет, создадим ее!
                if (diff > 0 && course.socialGroups.length <= 0) {
                    console.log('The time has come to start the marathon!');
                }
            }
        });

};