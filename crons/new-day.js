var moment = require('moment'),
    _ = require('underscore'),

    env = require('../env'),
    appConfig = require('../config/' + env + '/app'),
    User = require('../app/models/user'),
    Course = require('../app/models/course'),
    Checklist = require('../app/models/checklist'),
    Program = require('../app/models/program');


/**
 * Сохраняем чеклист, затем добавляем пользователю день
 *
 * @param userModel
 * @param checklist
 * @param newDay
 * @param quest
 * @param courseKey
 */
function addUserDay(userModel, checklist, newDay, quest, courseKey) {
    var checklistModel = new Checklist(checklist);
    checklistModel
        .saveQ()
        .spread(function (checklist) {
            userModel.courses[courseKey].days.push({
                dayNumber: newDay,
                quest: quest,
                checklist: checklist._id
            });
            userModel.saveQ()
                .spread(function (u) {
                    console.log('added new day for user with _id = ' + user._id);
                })
                .done();
        });
}

/**
 * Проверяем, доступен ли пользователю новый день
 *
 * @param user
 * @param courseDef
 */
function checkUserDays(user, courseDef) {
    var dateStart = moment(courseDef.dateStart).format();
    var userDay = 1 + moment().add(user.profile.timeZone, 'h').diff(dateStart, 'days');
    var courseK;
    for (var k in user.courses) {
        if (String(user.courses[k].course) === String(courseDef._id)) {
            courseK = k;
            break;
        }
    }
    if (!courseK) {
        throw 'Не могу найти курс у пользователя';
    }

    var days = user.courses[courseK].days ? user.courses[courseK].days : [];
    if (userDay > days.length) {
        //получаем последний день, лучше бы перебирать и искать максимальный day
        var prevDay = days[days.length - 1];
        prevDay = prevDay ? prevDay : {dayNumber: 0};
        var newDayNumber = prevDay.dayNumber + 1;

        var programDays = courseDef.program.days;
        for (var key in programDays) {
            if (programDays[key].dayNumber === newDayNumber) {
                var chl = {
                    user: user._id,
                    course: courseDef._id,
                    dayNumber: newDayNumber,
                    list: {
                        mandatory: [],
                        pinned: [],
                        simple: []
                    }
                };
                // Здесь все происходит через length
                // потому как это объект с кучей свойств
                if (programDays[key].checklist.list && programDays[key].checklist.list.mandatory) {
                    for (var i = 0; i < programDays[key].checklist.list.mandatory.length; i++) {
                        chl.list.mandatory.push(programDays[key].checklist.list.mandatory[i]);
                    }
                }
                if (prevDay.checklist && prevDay.checklist.list.pinned) {
                    for (var j = 0; j < prevDay.checklist.list.pinned.length; j++) {
                        var check = prevDay.checklist.list.pinned[j];
                        chl.list.pinned.push({
                            text: check.text,
                            isDone: false
                        });
                    }
                }

                addUserDay(user, chl, newDayNumber, programDays[key].quest, courseK);
                break;
            }
        }
        //TODO check if user has less than 3 error notifications
    }
}

function checkUserCourse(user, courseId) {
    Course
        .findById(courseId)
        .populate('program', 'days.dayNumber days.checklist days.quest')
        .execQ()
        .then(function (coursesDef) {
            checkUserDays(user, coursesDef);
        });

}

module.exports = function () {
    User
        .find()
        .populate('courses.days.checklist')
        .execQ()
        .then(function (users) {
            for (var i in users) {
                var user = users[i];
                for (var j in user.courses) {
                    checkUserCourse(user, user.courses[j].course);
                }
            }
        });

};